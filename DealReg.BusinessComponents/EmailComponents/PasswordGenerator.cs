﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DealReg.BusinessComponents.EmailComponents
{
    public class PasswordGenerator
    {
        public static string GeneratePassword(bool includeLowercase, bool includeUppercase, bool includeNumeric,
            bool includeSpecial, bool includeSpaces, int lengthOfPassword)
        {
            const int MaximumIdenticalConsecutiveChars = 2;
            const string LowercaseCharacters = "abcdefghijklmnopqrstuvwxyz";
            const string UppercaseCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string NumericCharacters = "0123456789";
            const string SpecialCharacters = @"!#$%&*@\";
            const string SpaceCharacter = " ";
            const int PasswordLengthMin = 8;
            const int PasswordlengthMax = 128;

            if (lengthOfPassword < PasswordLengthMin || lengthOfPassword > PasswordlengthMax)
            {
                return "Password length must be between 8 and 128.";
            }

            string characterSet = "";

            if (includeLowercase)
            {
                characterSet += LowercaseCharacters;
            }

            if (includeUppercase)
            {
                characterSet += UppercaseCharacters;
            }

            if (includeNumeric)
            {
                characterSet += NumericCharacters;
            }

            if (includeSpecial)
            {
                characterSet += SpecialCharacters;
            }

            if (includeSpaces)
            {
                characterSet += SpaceCharacter;
            }

            char[] password = new char[lengthOfPassword];
            int characterSetLength = characterSet.Length;

            System.Random random = new System.Random();
            for (int characterPosition = 0; characterPosition < lengthOfPassword; characterPosition++)
            {
                password[characterPosition] = characterSet[random.Next(characterSetLength - 1)];

                bool moreThanTwoIdenticalInARow =
                    characterPosition > MaximumIdenticalConsecutiveChars
                    && password[characterPosition] == password[characterPosition - 1]
                    && password[characterPosition - 1] == password[characterPosition - 2];

                if (moreThanTwoIdenticalInARow)
                {
                    characterPosition--;
                }
            }

            return string.Join(null, password);
        }

        public static bool PasswordIsValid(bool includeLowercase, bool includeUppercase, bool includeNumeric, bool includeSpecial,
            bool includeSpaces, string password)
        {
            const string REGEX_LOWERCASE = @"[a-z]";
            const string REGEX_UPPERCASE = @"[A-Z]";
            const string REGEX_NUMERIC = @"[\d]";
            const string REGEX_SPECIAL = @"([!#$%&*@\\])+";
            const string REGEX_SPACE = @"([ ])+";

            bool lowerCaseIsValid = !includeLowercase || (includeLowercase && Regex.IsMatch(password, REGEX_LOWERCASE));
            bool upperCaseIsValid = !includeUppercase || (includeUppercase && Regex.IsMatch(password, REGEX_UPPERCASE));
            bool numericIsValid = !includeNumeric || (includeNumeric && Regex.IsMatch(password, REGEX_NUMERIC));
            bool symbolsAreValid = !includeSpecial || (includeSpecial && Regex.IsMatch(password, REGEX_SPECIAL));
            bool spacesAreValid = !includeSpaces || (includeSpaces && Regex.IsMatch(password, REGEX_SPACE));

            return lowerCaseIsValid && upperCaseIsValid && numericIsValid && symbolsAreValid && spacesAreValid;
        }
    }
}
