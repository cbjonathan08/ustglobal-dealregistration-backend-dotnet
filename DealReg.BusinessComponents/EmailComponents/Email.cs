﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;

namespace DealReg.BusinessComponents.EmailComponents
{
    public class Email
    {

        private static readonly string EmailHost = ConfigurationManager.AppSettings["EmailHost"].ToString().Trim();
        private const int EmailPort = 587;//465
        private const string EmailAccount = "ruman143@gmail.com";
        private const string EmailPassword = "UstGlobal1";
        private const string EmailFrom = "ruman143@gmail.com";
        private const Boolean useSSL = true;
        private List<string> EmailTo { get; set; }
        private List<string>  EmailCC { get; set; }
        private string EmailSubject { get; set; }
        private string EmailContent { get; set; }

        public void setEmailDetails(string subject, string content, List<string> tos, List<string> ccs)
        {
            this.EmailSubject = subject;
            this.EmailContent = content;
            this.EmailTo = tos;
            this.EmailCC = ccs;
        }

        public Boolean sendEmail()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient(EmailHost);

                if (EmailTo == null && EmailTo.Count <= 0) return false;
                mail.From = new MailAddress(EmailFrom);
                EmailTo.ForEach(to => mail.To.Add(to));
                if (EmailCC != null && EmailCC.Count > 0)
                {
                    EmailCC.ForEach(to => mail.To.Add(to));
                }
                mail.Subject = EmailSubject;
                mail.Body = EmailContent;

                smtpServer.Port = EmailPort;
                smtpServer.Credentials = new System.Net.NetworkCredential(EmailAccount, EmailPassword);
                smtpServer.EnableSsl = useSSL;

                smtpServer.Send(mail);
                return true;
            }catch(Exception e)
            {
                return false;
            }
            
        }
    }
}
