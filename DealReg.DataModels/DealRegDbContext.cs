﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using DealReg.Entities.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DealReg.DataModels
{
    public class DealRegDbContext: DbContext
    {
        public DealRegDbContext() : base("DealRegDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DealRegDbContext, DealReg.DataModels.Migrations.Configuration>());

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermissions> RolePermissions { get; set; }
        public DbSet<UserRegistration> UserRegistration { get; set; }
        public DbSet<Geo> Geo { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<TimeLine> TimeLine { get; set; }
        public DbSet<Clients> Clients { get; set; }
        public DbSet<DealStatus> DealStatus { get; set; }
        public DbSet<DealRegistration> DealRegistration { get; set; }
        public DbSet<DealRegStatusHistory> DealRegStatusHistory { get; set; }
        public DbSet<DealRegComments> DealRegComments { get; set; }
        public DbSet<DealRegSubscribers> DealRegSubscribers { get; set; }
        public DbSet<Sla> Sla { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
