﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DealReg.Entities.Entities;

namespace DealReg.DataModels
{
    public partial class UnitOfWork : IUnitOfWork
    {
        private readonly DealRegDbContext _context;

        private IRepository<Role> _RoleRepository;
        private IRepository<RolePermissions> _RolePermissionsRepository;
        private IRepository<UserRegistration> _UserRegistrationRepository;
        private IRepository<Geo> _GeoRepository;
        private IRepository<Country> _CountryRepository;
        private IRepository<State> _StateRepository;
        private IRepository<TimeLine> _TimeLineRepository;
        private IRepository<Clients> _ClientsRepository;
        private IRepository<DealStatus> _DealStatusRepository;
        private IRepository<DealRegistration> _DealRegistrationRepository;
        private IRepository<DealRegStatusHistory> _DealRegStatusHistoryRepository;
        private IRepository<DealRegComments> _DealRegCommentsRepository;
        private IRepository<DealRegSubscribers> _DealRegSubscribersRepository;
        private IRepository<Project> _ProjectRepository;
        private IRepository<Sla> _SlaRepository;

        public IRepository<Role> RoleRepository
        {
            get
            {
                if (_RoleRepository == null)
                    _RoleRepository = new Repository<Role>(_context);

                return _RoleRepository;
            }
        }

        public IRepository<Project> ProjectRepository
        {
            get
            {
                if (_ProjectRepository == null)
                    _ProjectRepository = new Repository<Project>(_context);

                return _ProjectRepository;
            }
        }
        public IRepository<RolePermissions> RolePermissionsRepository
        {
            get
            {
                if (_RolePermissionsRepository == null)
                    _RolePermissionsRepository = new Repository<RolePermissions>(_context);

                return _RolePermissionsRepository;
            }
        }
        public IRepository<UserRegistration> UserRegistrationRepository
        {
            get
            {
                if (_UserRegistrationRepository == null)
                    _UserRegistrationRepository = new Repository<UserRegistration>(_context);

                return _UserRegistrationRepository;
            }
        }
        public IRepository<Geo> GeoRepository
        {
            get
            {
                if (_GeoRepository == null)
                    _GeoRepository = new Repository<Geo>(_context);

                return _GeoRepository;
            }
        }
        public IRepository<Country> CountryRepository
        {
            get
            {
                if (_CountryRepository == null)
                    _CountryRepository = new Repository<Country>(_context);

                return _CountryRepository;
            }
        }
        public IRepository<State> StateRepository
        {
            get
            {
                if (_StateRepository == null)
                    _StateRepository = new Repository<State>(_context);

                return _StateRepository;
            }
        }
        public IRepository<Sla> SlaRepository
        {
            get
            {
                if (_SlaRepository == null)
                    _SlaRepository = new Repository<Sla>(_context);

                return _SlaRepository;
            }
        }
        public IRepository<TimeLine> TimeLineRepository
        {
            get
            {
                if (_TimeLineRepository == null)
                    _TimeLineRepository = new Repository<TimeLine>(_context);

                return _TimeLineRepository;
            }
        }
        public IRepository<Clients> ClientsRepository
        {
            get
            {
                if (_ClientsRepository == null)
                    _ClientsRepository = new Repository<Clients>(_context);

                return _ClientsRepository;
            }
        }
        public IRepository<DealStatus> DealStatusRepository
        {
            get
            {
                if (_DealStatusRepository == null)
                    _DealStatusRepository = new Repository<DealStatus>(_context);

                return _DealStatusRepository;
            }
        }
        public IRepository<DealRegistration> DealRegistrationRepository
        {
            get
            {
                if (_DealRegistrationRepository == null)
                    _DealRegistrationRepository = new Repository<DealRegistration>(_context);

                return _DealRegistrationRepository;
            }
        }
        public IRepository<DealRegStatusHistory> DealRegStatusHistoryRepository
        {
            get
            {
                if (_DealRegStatusHistoryRepository == null)
                    _DealRegStatusHistoryRepository = new Repository<DealRegStatusHistory>(_context);

                return _DealRegStatusHistoryRepository;
            }
        }
        public IRepository<DealRegComments> DealRegCommentsRepository
        {
            get
            {
                if (_DealRegCommentsRepository == null)
                    _DealRegCommentsRepository = new Repository<DealRegComments>(_context);

                return _DealRegCommentsRepository;
            }
        }
        public IRepository<DealRegSubscribers> DealRegSubscribersRepository
        {
            get
            {
                if (_DealRegSubscribersRepository == null)
                    _DealRegSubscribersRepository = new Repository<DealRegSubscribers>(_context);

                return _DealRegSubscribersRepository;
            }
        }

        public UnitOfWork(DealRegDbContext _context)
        {
            this._context = _context;
        }
        public UnitOfWork()
        {
            _context = new DealRegDbContext();
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
