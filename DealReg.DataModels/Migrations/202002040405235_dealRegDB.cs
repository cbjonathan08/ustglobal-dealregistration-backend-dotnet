﻿namespace DealReg.DataModels.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dealRegDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "gbl.tbl_Clients",
                c => new
                    {
                        ClientID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150, unicode: false),
                        Address = c.String(nullable: false, maxLength: 500, unicode: false),
                        ContactPerson = c.String(maxLength: 150, unicode: false),
                        GeoID = c.Int(nullable: false),
                        CountryID = c.Int(nullable: false),
                        StateID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ClientID)
                .ForeignKey("ref.tbl_Country", t => t.CountryID)
                .ForeignKey("ref.tbl_Geo", t => t.GeoID)
                .ForeignKey("ref.tbl_State", t => t.StateID)
                .Index(t => t.GeoID)
                .Index(t => t.CountryID)
                .Index(t => t.StateID);
            
            CreateTable(
                "ref.tbl_Country",
                c => new
                    {
                        CountryID = c.Int(nullable: false, identity: true),
                        CountryName = c.String(nullable: false, maxLength: 100, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.CountryID);
            
            CreateTable(
                "ref.tbl_Geo",
                c => new
                    {
                        GeoID = c.Int(nullable: false, identity: true),
                        GeoName = c.String(nullable: false, maxLength: 100, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.GeoID);
            
            CreateTable(
                "ref.tbl_State",
                c => new
                    {
                        StateID = c.Int(nullable: false, identity: true),
                        CountryID = c.Int(nullable: false),
                        StateName = c.String(nullable: false, maxLength: 100, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.StateID)
                .ForeignKey("ref.tbl_Country", t => t.CountryID)
                .Index(t => t.CountryID);
            
            CreateTable(
                "out.tbl_DealRegComments",
                c => new
                    {
                        DealRegCommentsID = c.Int(nullable: false, identity: true),
                        DealRegID = c.Int(nullable: false),
                        Comments = c.String(nullable: false, maxLength: 500, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DealRegCommentsID)
                .ForeignKey("out.tbl_DealRegistration", t => t.DealRegID)
                .Index(t => t.DealRegID);
            
            CreateTable(
                "out.tbl_DealRegistration",
                c => new
                    {
                        DealRegID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        GeoID = c.Int(nullable: false),
                        CountryID = c.Int(nullable: false),
                        StateID = c.Int(nullable: false),
                        TimeLineID = c.Int(nullable: false),
                        DateOfReg = c.DateTime(nullable: false),
                        ClientID = c.Int(nullable: false),
                        ClientName = c.String(maxLength: 150, unicode: false),
                        ClientAddress = c.String(maxLength: 500, unicode: false),
                        ClientContactPerson = c.String(maxLength: 150, unicode: false),
                        UserComments = c.String(maxLength: 500, unicode: false),
                        IsActive = c.Boolean(nullable: false),
                        DealStatusID = c.Int(nullable: false),
                        ProjectID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DealRegID)
                .ForeignKey("ref.tbl_Country", t => t.CountryID)
                .ForeignKey("ref.tbl_DealStatus", t => t.DealStatusID)
                .ForeignKey("ref.tbl_Geo", t => t.GeoID)
                .ForeignKey("ref.tbl_Projects", t => t.ProjectID)
                .ForeignKey("ref.tbl_State", t => t.StateID)
                .ForeignKey("ref.tbl_TimeLine", t => t.TimeLineID)
                .ForeignKey("out.tbl_Users", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.GeoID)
                .Index(t => t.CountryID)
                .Index(t => t.StateID)
                .Index(t => t.TimeLineID)
                .Index(t => t.DealStatusID)
                .Index(t => t.ProjectID);
            
            CreateTable(
                "ref.tbl_DealStatus",
                c => new
                    {
                        DealStatusID = c.Int(nullable: false, identity: true),
                        DealStatusName = c.String(nullable: false, maxLength: 100, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DealStatusID);
            
            CreateTable(
                "ref.tbl_Projects",
                c => new
                    {
                        ProjectID = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(nullable: false, maxLength: 150, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProjectID);
            
            CreateTable(
                "ref.tbl_TimeLine",
                c => new
                    {
                        TimeLineID = c.Int(nullable: false, identity: true),
                        TimeLineName = c.String(nullable: false, maxLength: 100, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.TimeLineID);
            
            CreateTable(
                "out.tbl_Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        PhoneNumber = c.String(maxLength: 30),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("ref.tbl_Roles", t => t.RoleID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "ref.tbl_Roles",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 100, unicode: false),
                        RoleDesc = c.String(maxLength: 150, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "out.tbl_DealRegStatusHistory",
                c => new
                    {
                        DealRegStatusHistoryID = c.Int(nullable: false, identity: true),
                        DealRegID = c.Int(nullable: false),
                        DealStatusID = c.Int(nullable: false),
                        Comments = c.String(nullable: false, maxLength: 500, unicode: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DealRegStatusHistoryID)
                .ForeignKey("out.tbl_DealRegistration", t => t.DealRegID)
                .ForeignKey("ref.tbl_DealStatus", t => t.DealStatusID)
                .Index(t => t.DealRegID)
                .Index(t => t.DealStatusID);
            
            CreateTable(
                "out.tbl_DealRegSubscribers",
                c => new
                    {
                        DealRegSubscriberID = c.Int(nullable: false, identity: true),
                        DealRegID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.DealRegSubscriberID)
                .ForeignKey("out.tbl_DealRegistration", t => t.DealRegID)
                .ForeignKey("out.tbl_Users", t => t.UserID)
                .Index(t => t.DealRegID)
                .Index(t => t.UserID);
            
            CreateTable(
                "gbl.tbl_RolePermissions",
                c => new
                    {
                        RolePermissionID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        CanAdd = c.Boolean(nullable: false),
                        CanView = c.Boolean(nullable: false),
                        CanEdit = c.Boolean(nullable: false),
                        CanDelete = c.Boolean(nullable: false),
                        CanNotify = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.RolePermissionID)
                .ForeignKey("ref.tbl_Roles", t => t.RoleID)
                .Index(t => t.RoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("gbl.tbl_RolePermissions", "RoleID", "ref.tbl_Roles");
            DropForeignKey("out.tbl_DealRegSubscribers", "UserID", "out.tbl_Users");
            DropForeignKey("out.tbl_DealRegSubscribers", "DealRegID", "out.tbl_DealRegistration");
            DropForeignKey("out.tbl_DealRegStatusHistory", "DealStatusID", "ref.tbl_DealStatus");
            DropForeignKey("out.tbl_DealRegStatusHistory", "DealRegID", "out.tbl_DealRegistration");
            DropForeignKey("out.tbl_DealRegComments", "DealRegID", "out.tbl_DealRegistration");
            DropForeignKey("out.tbl_DealRegistration", "UserID", "out.tbl_Users");
            DropForeignKey("out.tbl_Users", "RoleID", "ref.tbl_Roles");
            DropForeignKey("out.tbl_DealRegistration", "TimeLineID", "ref.tbl_TimeLine");
            DropForeignKey("out.tbl_DealRegistration", "StateID", "ref.tbl_State");
            DropForeignKey("out.tbl_DealRegistration", "ProjectID", "ref.tbl_Projects");
            DropForeignKey("out.tbl_DealRegistration", "GeoID", "ref.tbl_Geo");
            DropForeignKey("out.tbl_DealRegistration", "DealStatusID", "ref.tbl_DealStatus");
            DropForeignKey("out.tbl_DealRegistration", "CountryID", "ref.tbl_Country");
            DropForeignKey("gbl.tbl_Clients", "StateID", "ref.tbl_State");
            DropForeignKey("ref.tbl_State", "CountryID", "ref.tbl_Country");
            DropForeignKey("gbl.tbl_Clients", "GeoID", "ref.tbl_Geo");
            DropForeignKey("gbl.tbl_Clients", "CountryID", "ref.tbl_Country");
            DropIndex("gbl.tbl_RolePermissions", new[] { "RoleID" });
            DropIndex("out.tbl_DealRegSubscribers", new[] { "UserID" });
            DropIndex("out.tbl_DealRegSubscribers", new[] { "DealRegID" });
            DropIndex("out.tbl_DealRegStatusHistory", new[] { "DealStatusID" });
            DropIndex("out.tbl_DealRegStatusHistory", new[] { "DealRegID" });
            DropIndex("out.tbl_Users", new[] { "RoleID" });
            DropIndex("out.tbl_DealRegistration", new[] { "ProjectID" });
            DropIndex("out.tbl_DealRegistration", new[] { "DealStatusID" });
            DropIndex("out.tbl_DealRegistration", new[] { "TimeLineID" });
            DropIndex("out.tbl_DealRegistration", new[] { "StateID" });
            DropIndex("out.tbl_DealRegistration", new[] { "CountryID" });
            DropIndex("out.tbl_DealRegistration", new[] { "GeoID" });
            DropIndex("out.tbl_DealRegistration", new[] { "UserID" });
            DropIndex("out.tbl_DealRegComments", new[] { "DealRegID" });
            DropIndex("ref.tbl_State", new[] { "CountryID" });
            DropIndex("gbl.tbl_Clients", new[] { "StateID" });
            DropIndex("gbl.tbl_Clients", new[] { "CountryID" });
            DropIndex("gbl.tbl_Clients", new[] { "GeoID" });
            DropTable("gbl.tbl_RolePermissions");
            DropTable("out.tbl_DealRegSubscribers");
            DropTable("out.tbl_DealRegStatusHistory");
            DropTable("ref.tbl_Roles");
            DropTable("out.tbl_Users");
            DropTable("ref.tbl_TimeLine");
            DropTable("ref.tbl_Projects");
            DropTable("ref.tbl_DealStatus");
            DropTable("out.tbl_DealRegistration");
            DropTable("out.tbl_DealRegComments");
            DropTable("ref.tbl_State");
            DropTable("ref.tbl_Geo");
            DropTable("ref.tbl_Country");
            DropTable("gbl.tbl_Clients");
        }
    }
}
