﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DealReg.Entities.Entities;

namespace DealReg.DataModels
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();

        T GetById(object id);

        void Add(T entity);

        void AddAll(List<T> entities);

        void Update(T entity);

        void Delete(T entity);

        UserRegistration GetByEmail(string email);
        UserRegistration IsValidUser(string email);
        UserRegistration GetUserDetails(string email, string password);
    }
}
