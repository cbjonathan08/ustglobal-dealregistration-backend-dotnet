﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Entity;
using DealReg.Entities.Entities;




using System.Diagnostics;

using System.Data;
using System.Reflection;

namespace DealReg.DataModels
{
    public class Repository<T> : IRepository<T> where T : class
    {
        internal DealRegDbContext context;
        internal DbSet<T> dbset;

        public Repository(DealRegDbContext context)
        {
            this.context = context;
            dbset = context.Set<T>();
        }

        public void Add(T entity)
        {
            dbset.Add(entity);
        }

        public void AddAll(List<T> entities)
        {
            dbset.AddRange(entities);
        }

        public void Delete(T entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
        }

        public IQueryable<T> GetAll()
        {
            return dbset;
        }

        public T GetById(object id)
        {
            return dbset.Find(id);
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public UserRegistration IsValidUser(string email)
        {
            UserRegistration regObj = context.UserRegistration.Where(u => u.Email == email).Include("Role").FirstOrDefault();
            return regObj;
        }
        public UserRegistration GetByEmail(string email)
        {
            UserRegistration regObj = context.UserRegistration.Where(u => u.Email.Equals(email)).FirstOrDefault();
            return regObj;
        }

        public UserRegistration GetUserDetails(string email, string password)
        {
            UserRegistration regObj = context.UserRegistration.Where(u => u.Email == email && u.Password == password).FirstOrDefault();
            return regObj;
        }


        //public list<UserRegistration> entitynames()
        //{
        //    list<dealregistration> lst_sub_category = new list<dealregistration>();
        //    lst_sub_category = context.userregistration.fromsql("usp_getdealstatusname").tolist();
        //    return lst_sub_category;
        //}


    }
}
