﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DealReg.Entities.Entities;

namespace DealReg.DataModels
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Role> RoleRepository { get; }
        IRepository<RolePermissions> RolePermissionsRepository { get; }
        IRepository<UserRegistration> UserRegistrationRepository { get; }
        IRepository<Geo> GeoRepository { get; }
        IRepository<Country> CountryRepository { get; }
        IRepository<State> StateRepository { get; }
        IRepository<TimeLine> TimeLineRepository { get; }
        IRepository<Clients> ClientsRepository { get; }
        IRepository<DealStatus> DealStatusRepository { get; }

        IRepository<DealRegistration> DealRegistrationRepository { get; }
        IRepository<DealRegStatusHistory> DealRegStatusHistoryRepository { get; }
        IRepository<DealRegComments> DealRegCommentsRepository { get; }
        IRepository<DealRegSubscribers> DealRegSubscribersRepository { get; }

        IRepository<Project> ProjectRepository { get; }
        IRepository<Sla> SlaRepository { get; }


        void Save();
    }
}
