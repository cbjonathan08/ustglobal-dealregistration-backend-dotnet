﻿CREATE PROCEDURE [dbo].[usp_GetMasterViewTable]( @UserId int, @Type nvarchar(50))  
AS  
BEGIN  
   -- BEGIN TRANSACTION         
   SET NOCOUNT ON;    
   IF  @Type LIKE '%Role%'  
             BEGIN  
             --LIST OF ROLE  
                SELECT TR.RoleID,TR.RoleName,TR.RoleDesc,TU.UserID, TU.FirstName as CreatedBy,
				TR.CreatedOn, tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn from [ref].[tbl_Roles] TR  
				JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID 
				JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
				 END  
               
  
       ELSE IF @Type LIKE '%Geo%'BEGIN  
             --LIST OF GEO  
             SELECT TR.GeoName,TR.GeoID,TU.UserID,TU.FirstName as CreatedBy,TR.CreatedOn, 
			  tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn 
			 from [ref].[tbl_Geo] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID 
			 JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
			 END  
  
       ELSE IF @Type LIKE '%Country%'BEGIN  
             --LIST OF COUNTRY  
             SELECT TR.CountryName as countryName,CountryID as countryID ,TU.FirstName as CreatedBy,TR.CreatedOn,TU.UserID,
			 tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn from [ref].[tbl_Country] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID 
			  JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
			 END  
  
       ELSE IF @Type LIKE '%State%'BEGIN  
             --LIST OF STATE  
             SELECT TC.CountryName, TR.StateId as stateID,TR.CountryID as countryID, TR.StateName as stateName,TU.FirstName as CreatedBy,
			 TR.CreatedOn,TU.UserID, tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn 
			 from [ref].[tbl_State] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID  
             JOIN [ref].[tbl_Country] TC ON TR.CountryID=TC.CountryID
			 JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID END  
  
       ELSE IF @Type LIKE '%TIMELINE%'BEGIN  
             --LIST OF TIMELINE  
             SELECT TR.TimeLineName as timeLineName,TR.TimeLineID as timeLineID,TU.FirstName as CreatedBy,TR.CreatedOn,TU.UserID,
			 tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn from [ref].tbl_TimeLine TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID 
			 JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
			 END  
  
       ELSE IF @Type LIKE '%CLIENT%'BEGIN  
             --LIST OF CLIENT  
				Select C.Name as name,C.Address as address,C.ClientID as clientID,C.CountryID as countryID, C.ContactPerson as contactPerson,
				C.GeoID as geoID,GeoName,TC.CountryName,C.StateID as stateID,TS.StateName,TU.FirstName as CreatedBy,
				C.CreatedOn,TX.FirstName as ModifiedBy,c.ModifiedBy as UserModifiedBy,TU.UserID,C.ModifiedOn  
				FROM [gbl].[tbl_Clients] C  
				JOIN [out].[tbl_Users] TU ON C.CreatedBy=TU.UserID  
				JOIN [ref].[tbl_Geo] TG ON C.GeoID=TG.GeoID  
				JOIN [ref].tbl_Country TC ON C.CountryID=TC.CountryID  
				LEFT JOIN [ref].tbl_State TS ON C.StateID=TS.StateID
				JOIN [out].[tbl_Users] TX ON C.ModifiedBy=TX.UserID
			 END  
  
       ELSE IF @Type LIKE '%PROJECT%'BEGIN  
             --LIST OF PROJECT  
             SELECT TR.ProjectID as projectID,TR.ProjectName as projectName,TU.FirstName as CreatedBy,TR.CreatedOn,
			  tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,
			 TU.UserID,TR.ModifiedBy,TR.ModifiedOn from [ref].[tbl_Projects] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID
			  JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
			  END  
  
       ELSE IF @Type LIKE '%STATUS%' BEGIN  
  
             --LIST OF DEAL STATUS  
             SELECT TR.DealStatusID as dealStatusID,TR.DealStatusName as dealStatusName,TU.FirstName as CreatedBy,
			 TR.CreatedOn,TU.UserID,  tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn from [ref].[tbl_DealStatus] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID
			 JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID
			 END  
	  
	  ELSE IF @Type LIKE '%SLA%' BEGIN  
  
             --LIST OF SLA
            SELECT TC.RoleName, TR.SlaID as SlaID,TR.RoleID as RoleID, TR.SLATIME as SlaTime,TU.FirstName as CreatedBy,
			 TR.CreatedOn,TU.UserID, tr.ModifiedBy as UserModifiedId,TX.FirstName as ModifiedBy,TR.ModifiedOn 
			 from [ref].[tbl_Sla] TR  
             JOIN [out].[tbl_Users] TU ON Tr.CreatedBy=TU.UserID  
             JOIN [ref].[tbl_Roles] TC ON TR.RoleID=TC.RoleID
			 JOIN [out].[tbl_Users] TX ON TR.ModifiedBy=TX.UserID END  
   
       ELSE  
       BEGIN  
         SELECT 20 as Code, 'Type Invalid' as description  
       END  
END