﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace DealReg.WebApi
{
    public class CustomAuthenticationMiddleware : OwinMiddleware
    {
        public CustomAuthenticationMiddleware(OwinMiddleware next) : base(next) { }

        public override async Task Invoke(IOwinContext context)
        {
            context.Response.OnSendingHeaders(state =>
            {
                var response = (OwinResponse)state;

                if (!response.Headers.ContainsKey("AuthorizationResponse") && response.StatusCode != 400) return;

                response.Headers.Remove("AuthorizationResponse");
                response.StatusCode = 401;

            }, context.Response);

            await Next.Invoke(context);
        }
    }
}