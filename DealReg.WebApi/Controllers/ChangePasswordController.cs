﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class ChangePasswordController : ApiController
    {
        private IUnitOfWork _unitofwork;
        private List<UserRegistration> Result = new List<UserRegistration>();
        private UserRegistration _obj = new UserRegistration();
        public ChangePasswordController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public ChangePasswordController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpPut]
        [Route("api/{DealReg}/UpdatePassword")]
        public Boolean UpdatePassword([FromBody] UserRegistration registration)
        {
            _obj = _unitofwork.UserRegistrationRepository.GetById(registration.UserID);

            if (_obj != null)
            {
                try
                {
                    _obj.Password = registration.Password;
                    _obj.ModifiedBy = registration.ModifiedBy;
                    _obj.ModifiedOn = registration.ModifiedOn;
                    _unitofwork.UserRegistrationRepository.Update(_obj);
                    _unitofwork.Save();

                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            else
                return false;
        }
    }
}
