﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public ClientController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public ClientController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/Clients")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<ClientViewModel> ClientViewModel = new List<ClientViewModel>();

            foreach (var item in _unitofwork.ClientsRepository.GetAll().ToList())
            {
                ClientViewModel.Add(new ClientViewModel()
                {
                    ClientID = item.ClientID,
                    Name = item.Name,
                    Address = item.Address,
                    ContactPerson = item.ContactPerson,
                    GeoID = item.GeoID, 
                    CountryID = item.CountryID,
                    StateID = item.StateID,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(ClientViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/ClientByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetProjectByID(int ClientID)
        {

            Clients client = _unitofwork.ClientsRepository.GetById(ClientID);

            if (client != null)
            {
                ClientViewModel ClientViewModel = new ClientViewModel()
                {
                    ClientID = client.ClientID,
                    Name = client.Name,
                    Address = client.Address,
                    ContactPerson = client.ContactPerson,
                    GeoID = client.GeoID,
                    CountryID = client.CountryID,
                    StateID = client.StateID,
                    CreatedBy = client.CreatedBy,
                    CreatedOn = client.CreatedOn,
                    ModifiedBy = client.ModifiedBy,
                    ModifiedOn = client.ModifiedOn
                };
                return Ok(ClientViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewClient")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] ClientViewModel ClientViewModel)
        {
            try
            {
                Clients client = new Clients()
                {
                    Name = ClientViewModel.Name,
                    Address = ClientViewModel.Address,
                    ContactPerson = ClientViewModel.ContactPerson,
                    GeoID = ClientViewModel.GeoID,
                    CountryID = ClientViewModel.CountryID,
                    StateID = ClientViewModel.StateID,
                    CreatedBy = ClientViewModel.CreatedBy,
                    ModifiedBy = ClientViewModel.CreatedBy

                };

                _unitofwork.ClientsRepository.Add(client);
                _unitofwork.Save();

                Clients existingClient = _unitofwork.ClientsRepository.GetAll()
                    .Where(x => x.GeoID == ClientViewModel.GeoID && x.CountryID == ClientViewModel.CountryID 
                    && x.StateID == ClientViewModel.StateID && x.Name == ClientViewModel.Name).FirstOrDefault();

                ClientViewModel clientViewModel = new ClientViewModel()
                {
                    ClientID = existingClient.ClientID,
                    Name = existingClient.Name,
                    Address = existingClient.Address,
                    ContactPerson = existingClient.ContactPerson,
                    GeoID = existingClient.GeoID,
                    CountryID = existingClient.CountryID,
                    StateID = existingClient.StateID,
                    CreatedBy = existingClient.CreatedBy,
                    CreatedOn = existingClient.CreatedOn,
                    ModifiedBy = existingClient.ModifiedBy,
                    ModifiedOn = existingClient.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, clientViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteClient")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int ClientID)
        {
            try
            {
                if (ClientID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Clients recToDelete = new Clients() { ClientID = ClientID };

                _unitofwork.ClientsRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateClient")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] ClientViewModel ClientViewModel)
        {
            try
            {
                if (ClientViewModel.Name == null && ClientViewModel.Address == null && ClientViewModel.ContactPerson == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Clients clientUpdate = new Clients()
                    {
                        ClientID = ClientViewModel.ClientID,
                        Name = ClientViewModel.Name,
                        Address = ClientViewModel.Address,
                        ContactPerson = ClientViewModel.ContactPerson,
                        GeoID = ClientViewModel.GeoID,
                        CountryID = ClientViewModel.CountryID,
                        StateID = ClientViewModel.StateID,
                        CreatedBy = ClientViewModel.CreatedBy,
                        ModifiedBy = ClientViewModel.ModifiedBy

                    };

                    _unitofwork.ClientsRepository.Update(clientUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, clientUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
