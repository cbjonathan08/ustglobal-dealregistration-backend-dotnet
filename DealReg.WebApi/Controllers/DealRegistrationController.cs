﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.WebApi.ADOConnection;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class DealRegistrationController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public DealRegistrationController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public DealRegistrationController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpPost]
        [Route("api/{DealReg}/NewDealReg")]
        //[Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] DealRegistrationModel DealRegistrationModel)
        {
            try
            {

                if (DealRegistrationModel.customerType == "New")
                {

                    Clients client = new Clients()
                    {
                        Name = DealRegistrationModel.name,
                        Address = DealRegistrationModel.address,
                        ContactPerson = DealRegistrationModel.contactPerson,
                        GeoID = DealRegistrationModel.geoID,
                        CountryID = DealRegistrationModel.countryID,
                        StateID = DealRegistrationModel.stateID,
                        CreatedBy = DealRegistrationModel.CreatedBy

                    };

                    _unitofwork.ClientsRepository.Add(client);
                    _unitofwork.Save();

                    DealRegistration dealReg = new DealRegistration()
                    {

                        //DealRegID = 0,
                        UserID = DealRegistrationModel.CreatedBy,
                        GeoID = DealRegistrationModel.geoID,
                        CountryID = DealRegistrationModel.countryID,
                        StateID = DealRegistrationModel.stateID,
                        TimeLineID = DealRegistrationModel.timeLineID,
                        DateOfReg = DateTime.Now,
                        //ClientID = null,
                        ClientName = DealRegistrationModel.name,
                        ClientAddress = DealRegistrationModel.address,
                        ClientContactPerson = DealRegistrationModel.contactPerson,
                        UserComments = DealRegistrationModel.comments,
                        IsActive = true,
                        DealStatusID = 1, 
                        ProjectID = DealRegistrationModel.projectID,
                        SlaAdmin = DealRegistrationModel.slaAdmin,
                        SlaPartner = DealRegistrationModel.slaPartner,
                        StateName = DealRegistrationModel.StateName,
                        PeerEmails = DealRegistrationModel.PeerEmails,
                        CreatedBy = DealRegistrationModel.CreatedBy
                        //CreatedOn = 
                        //ModifiedBy=1
                        //ModifiedOn

                    };

                    _unitofwork.DealRegistrationRepository.Add(dealReg);
                    _unitofwork.Save();

                }

                else if (DealRegistrationModel.customerType == "Existing")
                {
                    Clients existingClient = _unitofwork.ClientsRepository.GetAll()
                   .Where(x => x.ClientID == DealRegistrationModel.clientID).FirstOrDefault();

                    DealRegistration dealReg = new DealRegistration()
                    {

                        //DealRegID = 0,
                        UserID = DealRegistrationModel.CreatedBy,
                        GeoID = existingClient.GeoID,
                        CountryID = existingClient.CountryID,
                        StateID = existingClient.StateID,
                        TimeLineID = DealRegistrationModel.timeLineID,
                        DateOfReg = DateTime.Now,
                        ClientID = existingClient.ClientID,
                        ClientName = existingClient.Name,
                        ClientAddress = existingClient.Address,
                        ClientContactPerson = existingClient.ContactPerson,
                        UserComments = DealRegistrationModel.comments,
                        IsActive = true,
                        DealStatusID = 1, //where can i get this dealstatusID
                        ProjectID = DealRegistrationModel.projectID,
                        SlaAdmin = DealRegistrationModel.slaAdmin,
                        SlaPartner = DealRegistrationModel.slaPartner,
                        StateName = DealRegistrationModel.StateName,
                        PeerEmails = DealRegistrationModel.PeerEmails,
                        CreatedBy = DealRegistrationModel.CreatedBy
                        //CreatedOn = 
                        //ModifiedBy=1
                        //ModifiedOn

                    };

                    _unitofwork.DealRegistrationRepository.Add(dealReg);
                    _unitofwork.Save();

                }
                string error = "";
                DealsADOConnectionDAL dealADOobj = new DealsADOConnectionDAL();
                dealADOobj.ADO_SendMailToAdmin(DealRegistrationModel, ref error);
                return Request.CreateResponse(HttpStatusCode.Created, DealRegistrationModel); 
            }
            catch (Exception error)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error); 
            }
        }


        [HttpGet]
        [Route("api/{DealReg}/LoadDealReg")]
        [Authorize(Roles = "Admin")]
        public object Get()
        {
            List<DealRegistrationModel> DealRegistrationModel = new List<DealRegistrationModel>();

            var dealRegistartionRecord = _unitofwork.DealRegistrationRepository.GetAll().ToList();

         
            //repository.[tablename].ToList();

            return Ok(dealRegistartionRecord);
        }


    }
}