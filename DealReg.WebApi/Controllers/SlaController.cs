﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class SlaController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public SlaController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public SlaController()
        {
            this._unitofwork = new UnitOfWork();
        }
        [HttpGet]
        [Route("api/{DealReg}/Sla")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<SlaViewModel> SlaViewModel = new List<SlaViewModel>();

            foreach (var item in _unitofwork.SlaRepository.GetAll().ToList())
            {
                SlaViewModel.Add(new SlaViewModel()
                {
                    SlaID=item.SlaID,
                    RoleID=item.RoleID,
                    SLATIME=item.SLATIME,
                    CreatedBy=item.CreatedBy,
                    CreatedOn=item.CreatedOn,
                    ModifiedBy=item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(SlaViewModel);
        }

        [HttpPost]
        [Route("api/{DealReg}/NewSla")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] SlaViewModel SlaViewModel)
        {
            try
            {
                Sla sla = new Sla()
                {
                    RoleID = SlaViewModel.RoleID,
                    SLATIME = SlaViewModel.SLATIME,
                    CreatedBy = SlaViewModel.CreatedBy,
                    ModifiedBy = SlaViewModel.CreatedBy
                };

                _unitofwork.SlaRepository.Add(sla);
                _unitofwork.Save();

                Sla existingSla = _unitofwork.SlaRepository.GetAll()
                    .Where(x => x.SLATIME == SlaViewModel.SLATIME && x.RoleID == SlaViewModel.RoleID).FirstOrDefault();

                SlaViewModel slaViewModel = new SlaViewModel()
                {
                    RoleID = existingSla.RoleID,
                    SLATIME = existingSla.SLATIME,
                    CreatedBy = existingSla.CreatedBy,
                    CreatedOn = existingSla.CreatedOn,
                    ModifiedBy = existingSla.ModifiedBy,
                    ModifiedOn = existingSla.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, slaViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }
        [HttpGet]
        [Route("api/{DealReg}/DeleteSla")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int SlaID)
        {
            try
            {
                if (SlaID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Sla recToDelete = new Sla() { SlaID = SlaID };

                _unitofwork.SlaRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }
        [Route("api/{DealReg}/UpdateSla")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] SlaViewModel SlaViewModel)
        {
            try
            {
                if (SlaViewModel.SLATIME == null && SlaViewModel.RoleID == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Sla slaUpdate = new Sla()
                    {
                        SlaID = SlaViewModel.SlaID,
                        RoleID = SlaViewModel.RoleID,
                        SLATIME = SlaViewModel.SLATIME,
                        CreatedBy = SlaViewModel.CreatedBy,
                        ModifiedBy = SlaViewModel.ModifiedBy
                    };

                    _unitofwork.SlaRepository.Update(slaUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, slaUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
    }
}
