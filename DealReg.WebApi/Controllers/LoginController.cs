﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.Entities.Entities;
using DealReg.DataModels;
using System.Web.Http.Cors;
using System.Web.Http.Description;

using DealReg.WebApi.Models;
using System.Threading.Tasks;

namespace DealReg.WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class LoginController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public LoginController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public LoginController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpPost]
        [ResponseType(typeof(UserRegistration))]
        [Route("api/{DealReg}/GetLoginUserDetails")]
        public IHttpActionResult GetLoginUserDetails([FromBody] UserRegistration registration)
        {
            return Ok(_unitofwork.UserRegistrationRepository.GetAll().Where(x => x.Email == registration.Email && x.Password == registration.Password).OrderBy(x => x.UserID).ToList());
        }

        //[HttpPost]
        //[Route("api/{DealReg}/users/authenticate")]
        //public IHttpActionResult Authenticate([FromBody] LoginViewModel loginViewModel)
        //{
        //    if (loginViewModel.Username != null || loginViewModel.Password != null)
        //    {

        //    } else
        //    {
        //        // return BadRequest(new { message = "Username or password is incorrect" });

        //        ModelState.AddModelError("message", "Username or password is incorrect");
        //        return BadRequest(ModelState);
        //    }
        //}

        //[HttpPost]
        //[Route("api/{DealReg}/users/authenticate")]
        //public async Task<IActionResult> Authenticate([FromBody]LoginViewModel loginViewModel)
        //{
        //    //if (loginViewModel.Username != null || loginViewModel.Password != null)
        //    //{
        //    //    var user = await _usersService.Authenticate(loginViewModel);
        //    //    if (user == null)
        //    //        return BadRequest(new { message = "Username or password is incorrect" });

        //    //    HttpContext.User = await _applicationSignInManager.CreateUserPrincipalAsync(user);
        //    //    var tokens = _antiforgery.GetAndStoreTokens(HttpContext);
        //    //    Response.Headers.Add("Access-Control-Expose-Headers", "XSRF-REQUEST-TOKEN");
        //    //    Response.Headers.Add("XSRF-REQUEST-TOKEN", tokens.RequestToken);

        //    //    return Ok(user);
        //    //}
        //    //else
        //    //{
        //    //    return BadRequest(new { message = "Username or password is incorrect" });
        //    //}
        //}

        [HttpPost]
        [ResponseType(typeof(UserRegistration))]
        [Route("api/{DealReg}/GetLoginUserDetails1")]
        public IHttpActionResult GetLoginUserDetails1([FromBody] UserRegistration emp)
        {
            //try
            //{

            UserRegistration employee = _unitofwork.UserRegistrationRepository.IsValidUser(emp.Email);

            if (employee == null)
            {
                ModelState.AddModelError("", "Email id Does not Exist");
                return BadRequest(ModelState);
            }
            else if (employee.Password != emp.Password)
            {
                ModelState.AddModelError("", "Invalid Password");
                return BadRequest(ModelState);
            }
            else
            {
                return Ok(employee);
            }
            //}
            //catch (Exception ex)
            //{
            //    return InternalServerError(ex);
            //}
        }

        [HttpPost]
        [ResponseType(typeof(UserRegistration))]
        [Route("api/{DealReg}/GetLoginUserDetails2")]
        public HttpResponseMessage GetLoginUserDetails2([FromBody] UserRegistration emp)
        {
            //try
            //{

            UserRegistration employee = _unitofwork.UserRegistrationRepository.IsValidUser(emp.Email);

            if (employee == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid User.");
            }
            else if (employee.Password != emp.Password)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Password");
            }
            else
            {
                var message = Request.CreateResponse(HttpStatusCode.OK, employee);
                return message;
            }
            //}
            //catch (Exception ex)
            //{
            //    return InternalServerError(ex);
            //}
        }


        [ResponseType(typeof(UserRegistration))]
        [ActionName("RecoverPassword")]
        public IHttpActionResult Get(string empEmail)
        {
            // List<UserRegistration> employee = _unitofwork.RegistrationRepository.GetAll().Where(x => x.Email == empEmail).OrderBy(x => x.UserID).ToList();

            UserRegistration employee = _unitofwork.UserRegistrationRepository.IsValidUser(empEmail);

            if (employee == null)
            {
                ModelState.AddModelError("", "Email id Does not Exist");
                return BadRequest(ModelState);
            }
            else
            {
                return Ok(employee);
            }
        }
    }
}
