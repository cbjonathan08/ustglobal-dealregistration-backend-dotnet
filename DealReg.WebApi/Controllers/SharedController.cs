﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;
using DealReg.WebApi.ADOConnection;

namespace DealReg.WebApi.Controllers
{
    [AllowAnonymous]
    public class SharedController : ApiController
    {
        private IUnitOfWork _unitofwork;
        DealsADOConnectionDAL objDealsADOConnectionDAL = new DealsADOConnectionDAL();
        public SharedController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public SharedController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpPost]
        [Route("api/{DealReg}/NewMemberReg")]
        public HttpResponseMessage NewMemberRegistration([FromBody] UserViewModel registration)
        {
            try
            {
                //UserRegistration = class
                // user = variable

                // class variable = new class
                // Below is a way to use another class inside another class.
                UserRegistration user = new UserRegistration() { Email = registration.username };
                UserRegistration userExisit = _unitofwork.UserRegistrationRepository.GetByEmail(registration.username);
                if (userExisit != null)
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An account for the specified email address already exists. Try another email address.");

                if (registration.firstName == null ||  registration.username == null || registration.password == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");
                }
                if ((registration.firstName != null || registration.username != null || registration.password != null) && (registration.lastName == null))
                {
                    user.RoleID = Convert.ToInt32(registration.memberType);
                    user.FirstName = registration.firstName;
                    user.LastName = registration.firstName;
                    user.Password = registration.password;
                    user.PhoneNumber = "";
                    user.IsActive = true;
                    user.CreatedBy = registration.createdBy;

                    _unitofwork.UserRegistrationRepository.Add(user);
                    _unitofwork.Save();

                    string lsErrorMsg = "";
                    string mail_Body = "<p style='font-size:16px;'>Hi <b>" + registration.firstName + "</b>,</p>"
                        + "<b><p style='font-size:18px;color:black;'>Admin of Deal Registration had successfully registered you into the system.Your account details are as follow:</p></b>"
                            + "<p style='font-size:16px;'> Email Address : <b><span style='font-size:18px;color:blue;'>" + registration.username + "</span></b> </p>"
                            + "<p style='font-size:16px;'>Thank you</p>";

                    bool mailstatus = objDealsADOConnectionDAL.SendMail(registration.username, mail_Body, "Thanks for Registering at Deal Registration", ref lsErrorMsg);

                    var message = Request.CreateResponse(HttpStatusCode.Created, registration);
                    return message;
                }
                else
                {
                    user.RoleID = Convert.ToInt32(registration.memberType);
                    user.FirstName = registration.firstName;
                    user.LastName = registration.lastName;
                    user.Password = registration.password;
                    user.PhoneNumber = registration.contactNumber; 
                    user.IsActive = true;
                    user.CreatedBy = 0;

                    _unitofwork.UserRegistrationRepository.Add(user);
                    _unitofwork.Save();

                    string lsErrorMsg = "";
                    string mail_Body = "<p style='font-size:16px;'>Hi <b>" + registration.firstName + "</b>,</p>"
                        + "<b><p style='font-size:18px;color:black;'>Thank you for creating your account with Deal Registration. Your account details are as follow:</p></b>"
                            + "<p style='font-size:16px;'> Email Address: <b><span style='font-size:18px;color:blue;'>" + registration.username + "</span></b> </p>"
                            + "<p style='font-size:16px;'>Thank you</p>";

                    bool mailstatus = objDealsADOConnectionDAL.SendMail(registration.username, mail_Body, "Thanks for Registering at Deal Registration", ref lsErrorMsg);

                    var message = Request.CreateResponse(HttpStatusCode.Created, registration);
                    return message;
                }
            }

            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

      

        [HttpGet]
        [Route("api/{DealReg}/roles")]
        public object Get()
        {
            List<RoleViewModel> roles = new List<RoleViewModel>();

            foreach ( var item in _unitofwork.RoleRepository.GetAll().ToList())
            {
                roles.Add(new RoleViewModel() {
                    RoleID = item.RoleID,
                    RoleName = item.RoleName,
                    RoleDesc = item.RoleDesc,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            List<RoleViewModel> taskPriorities = roles;
            return taskPriorities;
        }
    }
}
