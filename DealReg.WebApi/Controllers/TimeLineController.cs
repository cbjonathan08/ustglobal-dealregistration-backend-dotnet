﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class TimeLineController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public TimeLineController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public TimeLineController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/TimeLines")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<TimeLineViewModel> TimeLineViewModel = new List<TimeLineViewModel>();

            foreach (var item in _unitofwork.TimeLineRepository.GetAll().ToList())
            {
                TimeLineViewModel.Add(new TimeLineViewModel()
                {
                    TimeLineID = item.TimeLineID,
                    TimeLineName = item.TimeLineName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(TimeLineViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/TimeLineByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetTimeLineByID(int TimeLineID)
        {

            TimeLine timeLine = _unitofwork.TimeLineRepository.GetById(TimeLineID);

            if (timeLine != null)
            {
                TimeLineViewModel TimeLineViewModel = new TimeLineViewModel()
                {
                    TimeLineID = timeLine.TimeLineID,
                    TimeLineName = timeLine.TimeLineName,
                    CreatedBy = timeLine.CreatedBy,
                    CreatedOn = timeLine.CreatedOn,
                    ModifiedBy = timeLine.ModifiedBy,
                    ModifiedOn = timeLine.ModifiedOn
                };
                return Ok(TimeLineViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewTimeLine")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] TimeLineViewModel TimeLineViewModel)
        {
            try
            {
                TimeLine timeLine = new TimeLine()
                {
                    TimeLineName = TimeLineViewModel.TimeLineName,
                    CreatedBy = TimeLineViewModel.CreatedBy,
                    ModifiedBy = TimeLineViewModel.CreatedBy,
                };

                _unitofwork.TimeLineRepository.Add(timeLine);
                _unitofwork.Save();

                TimeLine existingTimeLine = _unitofwork.TimeLineRepository.GetAll().Where(x => x.TimeLineName == TimeLineViewModel.TimeLineName).FirstOrDefault();

                TimeLineViewModel timeLineViewModel = new TimeLineViewModel()
                {
                    TimeLineID = existingTimeLine.TimeLineID,
                    TimeLineName = existingTimeLine.TimeLineName,
                    CreatedBy = existingTimeLine.CreatedBy,
                    CreatedOn = existingTimeLine.CreatedOn,
                    ModifiedBy = existingTimeLine.ModifiedBy,
                    ModifiedOn = existingTimeLine.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, TimeLineViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteTimeLine")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int TimeLineID)
        {
            try
            {
                if (TimeLineID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                TimeLine recToDelete = new TimeLine() { TimeLineID = TimeLineID };

                _unitofwork.TimeLineRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateTimeLine")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] TimeLineViewModel TimeLineViewModel)
        {
            try
            {
                if (TimeLineViewModel.TimeLineName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    TimeLine timeLineUpdate = new TimeLine()
                    {
                        TimeLineID = TimeLineViewModel.TimeLineID,
                        TimeLineName = TimeLineViewModel.TimeLineName,
                        CreatedBy = TimeLineViewModel.CreatedBy,
                        ModifiedBy = TimeLineViewModel.ModifiedBy
                    };

                    _unitofwork.TimeLineRepository.Update(timeLineUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, timeLineUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
