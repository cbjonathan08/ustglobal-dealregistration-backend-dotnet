﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class GeoController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public GeoController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public GeoController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/Geos")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<GeoViewModel> geoViewModel = new List<GeoViewModel>();

            foreach (var item in _unitofwork.GeoRepository.GetAll().ToList())
            {
                geoViewModel.Add(new GeoViewModel()
                {
                    GeoID = item.GeoID,
                    GeoName = item.GeoName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(geoViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/GeoByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetGeoByID(int GeoID)
        {

            Geo role = _unitofwork.GeoRepository.GetById(GeoID);

            if (role != null)
            {
                GeoViewModel geoViewModel = new GeoViewModel()
                {
                    GeoID = role.GeoID,
                    GeoName = role.GeoName,
                    CreatedBy = role.CreatedBy,
                    CreatedOn = role.CreatedOn,
                    ModifiedBy = role.ModifiedBy,
                    ModifiedOn = role.ModifiedOn
                };
                return Ok(geoViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewGeo")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] GeoViewModel GeoViewModel)
        {
            try
            {
                Geo geo = new Geo()
                {
                    GeoName = GeoViewModel.GeoName,
                    CreatedBy = GeoViewModel.CreatedBy,
                    ModifiedBy = GeoViewModel.CreatedBy
                };

                _unitofwork.GeoRepository.Add(geo);
                _unitofwork.Save();

                Geo existingGeo = _unitofwork.GeoRepository.GetAll().Where(x => x.GeoName == GeoViewModel.GeoName).FirstOrDefault();

                GeoViewModel geoViewModel = new GeoViewModel()
                {
                    GeoID = existingGeo.GeoID,
                    GeoName = existingGeo.GeoName,
                    CreatedBy = existingGeo.CreatedBy,
                    CreatedOn = existingGeo.CreatedOn,
                    ModifiedBy = existingGeo.ModifiedBy,
                    ModifiedOn = existingGeo.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, geoViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteGeo")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int GeoID)
        {
            try
            {
                if (GeoID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Geo recToDelete = new Geo() { GeoID = GeoID };

                _unitofwork.GeoRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/UpdateGeo")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage PostUpdate([FromBody] GeoViewModel GeoViewModel)
        {
            try
            {
                if (GeoViewModel.GeoName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Geo geoUpdate = new Geo() {
                        GeoID = GeoViewModel.GeoID,
                        GeoName = GeoViewModel.GeoName,
                        CreatedBy = GeoViewModel.CreatedBy,
                        ModifiedBy = GeoViewModel.ModifiedBy

                    };

                    _unitofwork.GeoRepository.Update(geoUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, geoUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
    }
}
