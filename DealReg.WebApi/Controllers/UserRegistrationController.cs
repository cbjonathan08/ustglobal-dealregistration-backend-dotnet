﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.DataModels;
using DealReg.Entities.Entities;
using DealReg.WebApi.ADOConnection;

namespace DealReg.WebApi.Controllers
{
    public class UserRegistrationController : ApiController
    {
        private IUnitOfWork _unitofwork;
        DealsADOConnectionDAL objDealsADOConnectionDAL = new DealsADOConnectionDAL();
        public UserRegistrationController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public UserRegistrationController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpPost]
        [Route("api/{DealReg}/SaveReg")]
        public HttpResponseMessage SaveRegistration([FromBody] UserRegistration registration)
        {
            try
            {
                UserRegistration user = new UserRegistration() { Email = registration.Email };

               // if(user != null)
                   // return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An account for the specified email address already exists. Try another email address.");

                if (registration.FirstName == null || registration.LastName == null || registration.Email == null || registration.Password == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");
                }
                else
                {
                    _unitofwork.UserRegistrationRepository.Add(registration);
                    _unitofwork.Save();
                    string lsErrorMsg = "";
                    string mail_Body = "<p style='font-size:16px;'>Hi <b>"+ registration.FirstName + "</b>,</p>" 
                        + "<b><p style='font-size:18px;color:black;'>Thank you for creating your account with Registration. Your account details are as follow:</p></b>"
                            + "<p style='font-size:16px;'> Email : <b><span style='font-size:18px;color:blue;'>" + registration.Email + "</span></b> </p>"
                            + "<p style='font-size:16px;'>Thank you</p>";

                    bool mailstatus = objDealsADOConnectionDAL.SendMail(registration.Email, mail_Body, "Thanks for Registering at Dealworld", ref lsErrorMsg);
                    var message = Request.CreateResponse(HttpStatusCode.Created, registration);
                    return message;
                }
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An account for the specified email address already exists. Try another email address.");
            }

            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/GetRegInfoByID/{id}")]
        public HttpResponseMessage GetRegistrationInformation(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "ID value must be more than 0");
                }
                else
                {
                    //the comment is to open back - Najah
                    // var message = _unitofwork.UserRegistrationRepository.GetAll().Where(x => x.UserID == id).OrderBy(x => x.UserID).ToList();
                    // return Request.CreateResponse(HttpStatusCode.OK, message);
                    return Request.CreateResponse(HttpStatusCode.OK, "testing respond");

                }

            }

            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error.Message);
            }
        }

        //to get all user details
        [HttpGet]
        [Route("api/{DealReg}/GetRegInfo")]
        public HttpResponseMessage GetRegistrationInformation()
        {
            try
            {
                var result = _unitofwork.UserRegistrationRepository.GetAll();

                if (result == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }

            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error);
            }
        }

        [HttpPut]
        [Route("api/{DealReg}/UpdateReg")]
        public HttpResponseMessage UpdateRegistration([FromBody] UserRegistration userUpdate)
        {
            try
            {
                if (userUpdate.FirstName == null || userUpdate.LastName == null || userUpdate.Email == null || userUpdate.Password == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    _unitofwork.UserRegistrationRepository.Update(userUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, userUpdate);
                    return message;
                }
            }

            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteUser/{ID}")]
        public HttpResponseMessage DeleteUser(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }
                else
                {
                    UserRegistration userToDelete = new UserRegistration() { UserID = id };
                    _unitofwork.UserRegistrationRepository.Delete(userToDelete);
                    _unitofwork.Save();
                    return Request.CreateResponse(HttpStatusCode.OK, true);
                }
            }

            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }


        [HttpPut]
        [Route("api/{DealReg}/UpdateStatus")]
        public HttpResponseMessage UpdateStat([FromBody] DealRegistration userStatusUpdateModel)
        {
            try
            {

                //Geo geoUpdate = new Geo()
                //{
                //    GeoID = GeoViewModel.GeoID,
                //    GeoName = GeoViewModel.GeoName
                //};

                //_unitofwork.DealRegistrationRepository.Update(geoUpdate);
                _unitofwork.Save();
                var message = Request.CreateResponse(HttpStatusCode.Created);
                return message;

            }

            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }



    }
}
