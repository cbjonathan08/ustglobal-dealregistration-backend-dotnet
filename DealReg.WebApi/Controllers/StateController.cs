﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;


namespace DealReg.WebApi.Controllers
{
    public class StateController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public StateController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public StateController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/States")]
        [Authorize(Roles = "Admin")]
        public object Get()
        {
            List<StateViewModel> StateViewModel = new List<StateViewModel>();

            foreach (var item in _unitofwork.StateRepository.GetAll().ToList())
            {
                StateViewModel.Add(new StateViewModel()
                {
                    StateID = item.StateID,
                    CountryID = item.CountryID,
                    StateName = item.StateName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(StateViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/StateByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetProjectByID(int StateID)
        {

            State state = _unitofwork.StateRepository.GetById(StateID);

            if (state != null)
            {
                StateViewModel StateViewModel = new StateViewModel()
                {
                    StateID = state.StateID,
                    CountryID = state.CountryID,
                    StateName = state.StateName,
                    CreatedBy = state.CreatedBy,
                    CreatedOn = state.CreatedOn,
                    ModifiedBy = state.ModifiedBy,
                    ModifiedOn = state.ModifiedOn
                };
                return Ok(StateViewModel);
            }
            else
                return null;
        }

        [HttpGet]
        [Route("api/{DealReg}/States")]
        //[Authorize(Roles = "Admin")]
        public object GetStatesByCountryID(int CountryID)
        {
            List<State> states = _unitofwork.StateRepository.GetAll().Where(x=>x.CountryID == CountryID).ToList();

            if (states != null)
                return Ok(states);
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewState")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] StateViewModel StateViewModel)
        {
            try
            {
                State state = new State()
                {
                    CountryID = StateViewModel.CountryID,
                    StateName = StateViewModel.StateName,
                    CreatedBy = StateViewModel.CreatedBy,
                    ModifiedBy = StateViewModel.CreatedBy
                };

                _unitofwork.StateRepository.Add(state);
                _unitofwork.Save();

                State existingState = _unitofwork.StateRepository.GetAll()
                    .Where(x => x.StateName == StateViewModel.StateName && x.CountryID == StateViewModel.CountryID).FirstOrDefault();

                StateViewModel stateViewModel = new StateViewModel()
                {
                    StateID = existingState.StateID,
                    CountryID = existingState.CountryID,
                    StateName = existingState.StateName,
                    CreatedBy = existingState.CreatedBy,
                    CreatedOn = existingState.CreatedOn,
                    ModifiedBy = existingState.ModifiedBy,
                    ModifiedOn = existingState.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, stateViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteState")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int StateID)
        {
            try
            {
                if (StateID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                State recToDelete = new State() { StateID = StateID };

                _unitofwork.StateRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateState")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] StateViewModel StateViewModel)
        {
            try
            {
                if (StateViewModel.StateName == null && StateViewModel.CountryID == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    State stateUpdate = new State()
                    {
                        StateID = StateViewModel.StateID,
                        CountryID = StateViewModel.CountryID,
                        StateName = StateViewModel.StateName,
                        CreatedBy = StateViewModel.CreatedBy,
                        ModifiedBy = StateViewModel.ModifiedBy
                    };

                    _unitofwork.StateRepository.Update(stateUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, stateUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
