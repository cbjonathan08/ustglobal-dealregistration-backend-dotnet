﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class ProjectController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public ProjectController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public ProjectController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/Projects")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<ProjectViewModel> ProjectViewModel = new List<ProjectViewModel>();

            foreach (var item in _unitofwork.ProjectRepository.GetAll().ToList())
            {
                ProjectViewModel.Add(new ProjectViewModel()
                {
                    ProjectID = item.ProjectID,
                    ProjectName = item.ProjectName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(ProjectViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/ProjectByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetProjectByID(int ProjectID)
        {

            Project country = _unitofwork.ProjectRepository.GetById(ProjectID);

            if (country != null)
            {
                ProjectViewModel ProjectViewModel = new ProjectViewModel()
                {
                    ProjectID = country.ProjectID,
                    ProjectName = country.ProjectName,
                    CreatedBy = country.CreatedBy,
                    CreatedOn = country.CreatedOn,
                    ModifiedBy = country.ModifiedBy,
                    ModifiedOn = country.ModifiedOn
                };
                return Ok(ProjectViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewProject")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] ProjectViewModel ProjectViewModel)
        {
            try
            {
                Project project = new Project()
                {
                    ProjectName = ProjectViewModel.ProjectName,
                    CreatedBy = ProjectViewModel.CreatedBy,
                    ModifiedBy = ProjectViewModel.CreatedBy
                };

                _unitofwork.ProjectRepository.Add(project);
                _unitofwork.Save();

                Project existingProject = _unitofwork.ProjectRepository.GetAll().Where(x => x.ProjectName == ProjectViewModel.ProjectName).FirstOrDefault();

                ProjectViewModel projectViewModel = new ProjectViewModel()
                {
                    ProjectID = existingProject.ProjectID,
                    ProjectName = existingProject.ProjectName,
                    CreatedBy = existingProject.CreatedBy,
                    CreatedOn = existingProject.CreatedOn,
                    ModifiedBy = existingProject.ModifiedBy,
                    ModifiedOn = existingProject.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, ProjectViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteProject")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int ProjectID)
        {
            try
            {
                if (ProjectID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Project recToDelete = new Project() { ProjectID = ProjectID };

                _unitofwork.ProjectRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateProject")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] ProjectViewModel ProjectViewModel)
        {
            try
            {
                if (ProjectViewModel.ProjectName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Project projectUpdate = new Project()
                    {
                        ProjectID = ProjectViewModel.ProjectID,
                        ProjectName = ProjectViewModel.ProjectName,
                        CreatedBy = ProjectViewModel.CreatedBy,
                        ModifiedBy = ProjectViewModel.ModifiedBy


                    };

                    _unitofwork.ProjectRepository.Update(projectUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, projectUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
