﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;
using DealReg.WebApi.ADOConnection;

namespace DealReg.WebApi.Controllers
{
    public class DealsADOController : ApiController
    {
        private List<ErrorLog> _ErrorLog = new List<ErrorLog>();
        DealsADOConnectionDAL objDealsADOConnectionDAL = new DealsADOConnectionDAL();
        public List<ErrorLog> ErrorLog
        {
            get { return _ErrorLog; }
            set { _ErrorLog = value; }
        }
        [HttpPost]
        [Route("api/{DealReg}/GetDealRegInfo_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> GetDealRegInfo_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                object loReturn = objDealsADOConnectionDAL.ADO_GetDealRegInfo(ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }
        [HttpPost]
        [Route("api/{DealReg}/GetDealRegInfoPartner_ADO")]
        [Authorize(Roles = "Partner Admin")]
        public List<ReturnData> GetDealRegInfoPartner_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                object loReturn = objDealsADOConnectionDAL.ADO_GetDealRegInfoPartner(ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }



        [HttpPost]
        [Route("api/{DealReg}/GetDealRegInfoWithComments_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> GetDealRegInfoWithComments_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                object loReturn = objDealsADOConnectionDAL.ADO_GetDealRegInfoWithComments(ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }


        [HttpPost]
        [Route("api/{DealReg}/UpdateUserComments_ADO")]
        //[Authorize(Roles = "Admin")]
        public List<ReturnData> UpdateUserComments_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.DealId == null || data.DealComment == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.DealId != null && data.DealId.ToString().Trim() == "")
                {
                    addErrorLog("DealId Missing. Please add all required parameters", "E");
                }
                if (data.DealComment != null && data.DealComment.ToString().Trim() == "")
                {
                    addErrorLog("DealComment Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_UpdateUserComments((data.DealId).ToString(), (data.DealComment).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/UpdateDealStatusID_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> UpdateDealStatusID_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                string lsErrorMsgCheck = "";
                string DealsStatusID = "2,3,4";
                if (data == null || data.DealIds == null || data.Status == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.DealIds != null && data.DealIds.ToString().Trim() == "")
                {
                    addErrorLog("DealIds Missing. Please add all required parameters", "E");
                }
                if (data.Status != null && data.Status.ToString().Trim() == "")
                {
                    addErrorLog("Status Missing. Please add all required parameters", "E");
                }
                if (data.SlaAdmin != null && data.SlaAdmin.ToString().Trim() == "")
                {
                    addErrorLog("SlaAdmin Missing. Please add all required parameters", "E");
                }
                if (data.SlaPartner != null && data.SlaPartner.ToString().Trim() == "")
                {
                    addErrorLog("SlaPartner Missing. Please add all required parameters", "E");
                }
                if (data.Comments != null && data.Comments.ToString().Trim() == "")
                {
                    addErrorLog("Comments Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturnCheck = objDealsADOConnectionDAL.ADO_DealStatusIDCheck((data.DealIds).ToString(), DealsStatusID.ToString(), ref lsErrorMsgCheck);
                if (lsErrorMsgCheck != "" && Convert.ToBoolean(loReturnCheck) == false)
                {
                    var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, lsErrorMsgCheck);
                    return uiReturnData(message);

                }
                object loReturn = objDealsADOConnectionDAL.ADO_UpdateDealStatusID((data.DealIds).ToString(), (data.Status).ToString(), (data.SlaAdmin).ToString(), (data.SlaPartner).ToString(), (data.Comments).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/UpdateDealStatusIDPartner_ADO")]
        [Authorize(Roles = "Partner Admin")]
        public List<ReturnData> UpdateDealStatusIDPartner_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                string lsErrorMsgCheck = "";
                string DealsStatusID = "2,3";
                if (data == null || data.DealIds == null || data.Status == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.DealIds != null && data.DealIds.ToString().Trim() == "")
                {
                    addErrorLog("DealIds Missing. Please add all required parameters", "E");
                }
                if (data.Status != null && data.Status.ToString().Trim() == "")
                {
                    addErrorLog("Status Missing. Please add all required parameters", "E");
                }
                if (data.Comments != null && data.Comments.ToString().Trim() == "")
                {
                    addErrorLog("Comments Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturnCheck = objDealsADOConnectionDAL.ADO_DealStatusIDCheck((data.DealIds).ToString(), DealsStatusID.ToString(), ref lsErrorMsgCheck);
                if (lsErrorMsgCheck != "" && Convert.ToBoolean(loReturnCheck) == false)
                {
                    var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, lsErrorMsgCheck);
                    return uiReturnData(message);

                }
                object loReturn = objDealsADOConnectionDAL.ADO_UpdateDealStatusIDPartner((data.DealIds).ToString(), (data.Status).ToString(), data.Comments.ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/GetDealMappedSubscribers_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> GetDealMappedSubscribers_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.DealRegID == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.DealRegID != null && data.DealRegID.ToString().Trim() == "")
                {
                    addErrorLog("DealRegID Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_GetDealMappedSubscribers((data.DealRegID).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/AddOrRemoveSubscribers_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> AddOrRemoveSubscribers_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.DealRegID == null || data.DealRegID == null || data.DealRegID == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.DealRegID != null && data.DealRegID.ToString().Trim() == "")
                {
                    addErrorLog("DealRegID Missing. Please add all required parameters", "E");
                }
                if (data.UserID != null && data.UserID.ToString().Trim() == "")
                {
                    addErrorLog("UserID Missing. Please add all required parameters", "E");
                }
                if (data.CreatedBy != null && data.CreatedBy.ToString().Trim() == "")
                {
                    addErrorLog("CreatedBy Missing. Please add all required parameters", "E");
                }
                if (data.Status != null && data.Status.ToString().Trim() == "")
                {
                    addErrorLog("Status Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_AddOrRemoveSubscribers((data.DealRegID).ToString(),
                    (data.UserID).ToString(), (data.Status).ToString(), (data.CreatedBy).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/GetDashboardData_ADO")]
        //[Authorize(Roles = "Admin,Partner Admin")]
        public List<ReturnData> GetDashboardData_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserID == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserID != null && data.UserID.ToString().Trim() == "")
                {
                    addErrorLog("UserID  Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_GetDashboardData((data.UserID).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }


        [HttpPost]
        [Route("api/{DealReg}/SendAMail")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> SendAMail([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.psTo == null || data.psBody == null || data.psSubject == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.psTo != null && data.psTo.ToString().Trim() == "")
                {
                    addErrorLog("psTo Missing. Please add all required parameters", "E");
                }
                if (data.psBody != null && data.psBody.ToString().Trim() == "")
                {
                    addErrorLog("psBody Missing. Please add all required parameters", "E");
                }
                if (data.psSubject != null && data.psSubject.ToString().Trim() == "")
                {
                    addErrorLog("psSubject Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.SendMail((data.psTo).ToString(), (data.psBody).ToString(), (data.psSubject).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }


        [HttpPost]
        [Route("api/{DealReg}/ForgotPassword_ADO")]
        //[Authorize(Roles = "Admin")]
        public List<ReturnData> ForgotPassword_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.emailID == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.emailID != null && data.emailID.ToString().Trim() == "")
                {
                    addErrorLog("emailID Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_ForgotPassword((data.emailID).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/GetAdminReportData_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> GetAdminReportData_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.Type == null || data.DealStatusID == null || data.StartDate == null || data.LastDate == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.Type != null && data.Type.ToString().Trim() == "")
                {
                    addErrorLog("Type  Missing. Please add all required parameters", "E");
                }
                if (data.DealStatusID != null && data.DealStatusID.ToString().Trim() == "")
                {
                    addErrorLog("DealStatusID  Missing. Please add all required parameters", "E");
                }
                if (data.StartDate != null && data.StartDate.ToString().Trim() == "")
                {
                    addErrorLog("StartDate   Missing. Please add all required parameters", "E");
                }
                if (data.LastDate != null && data.LastDate.ToString().Trim() == "")
                {
                    addErrorLog("LastDate   Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_GetAdminReportData((data.Type).ToString(), (data.DealStatusID).ToString(), (data.StartDate).ToString(),
                    (data.LastDate).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/GetMasterTableRecords_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> GetMasterTableRecords_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.Type == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.Type != null && data.Type.ToString().Trim() == "")
                {
                    addErrorLog("Type  Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_GetMasterTableRecords((data.UserId).ToString(), (data.Type).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/GetUserDashboard_ADO")]
        //[Authorize(Roles = "Member")]
        public List<ReturnData> GetUserDashboard_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_GetUserDashboard((data.UserId).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/UserDealFilterByID_ADO")]
        //[Authorize(Roles = "Member")]
        public List<ReturnData> UserDealFilterByID_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_UserDealFilterByID((data.UserId).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/ChangePassword_ADO")]
        public List<ReturnData> ChangePassword_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null || data.OldPassword == null || data.NewPassword == null || data.ModifiedBy == null || data.RoleType == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId  Missing. Please add all required parameters", "E");
                }
                if (data.OldPassword != null && data.OldPassword.ToString().Trim() == "")
                {
                    addErrorLog("OldPassword  Missing. Please add all required parameters", "E");
                }
                if (data.NewPassword != null && data.NewPassword.ToString().Trim() == "")
                {
                    addErrorLog("NewPassword   Missing. Please add all required parameters", "E");
                }
                if (data.ModifiedBy != null && data.ModifiedBy.ToString().Trim() == "")
                {
                    addErrorLog("ModifiedBy Missing. Please add all required parameters", "E");
                }
                if (data.RoleType != null && data.RoleType.ToString().Trim() == "")
                {
                    addErrorLog("RoleType Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_ChangePassword((data.UserId).ToString(), (data.OldPassword).ToString(), (data.NewPassword).ToString(),
                    (data.ModifiedBy).ToString(), (data.RoleType).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/AdminGetAllUsers_ADO")]
        //[Authorize(Roles = "Admin")]
        public List<ReturnData> AdminGetAllUsers_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_AdminGetAllUsers(ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/UpdateUserDetails_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> AdminUpdateUserDetails_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserID == null || data.FirstName == null || data.LastName == null 
                    || data.Email == null || data.PhoneNumber == null || data.IsActive == null )
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserID != null && data.UserID.ToString().Trim() == "")
                {
                    addErrorLog("UserId  Missing. Please add all required parameters", "E");
                }
                if (data.FirstName != null && data.FirstName.ToString().Trim() == "")
                {
                    addErrorLog("FirstName  Missing. Please add all required parameters", "E");
                }
                if (data.LastName != null && data.LastName.ToString().Trim() == "")
                {
                    addErrorLog("LastName   Missing. Please add all required parameters", "E");
                }
                if (data.Email != null && data.Email.ToString().Trim() == "")
                {
                    addErrorLog("Email Missing. Please add all required parameters", "E");
                }
                if (data.PhoneNumber != null && data.PhoneNumber.ToString().Trim() == "")
                {
                    addErrorLog("PhoneNumber Missing. Please add all required parameters", "E");
                }
                if (data.IsActive != null && data.IsActive.ToString().Trim() == "")
                {
                    addErrorLog("IsActive Missing. Please add all required parameters", "E");
                }
               
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_UpdateUserDetails((data.UserID).ToString(), (data.FirstName).ToString(), (data.LastName).ToString()
                    , (data.Email).ToString(), (data.PhoneNumber).ToString(), (data.IsActive).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/AdminDeleteUser_ADO")]
        [Authorize(Roles = "Admin")]
        public List<ReturnData> AdminDeleteUser_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_AdminDeleteUser((data.UserId).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/userGetDetails_ADO")]
        public List<ReturnData> userGetDetails_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_userGetDetails((data.UserId).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/userSingleUpdate_ADO")]
        public List<ReturnData> userSingleUpdate_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserID == null || data.FirstName == null || data.LastName == null
                    || data.Email == null || data.PhoneNumber == null || data.IsActive == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserID != null && data.UserID.ToString().Trim() == "")
                {
                    addErrorLog("UserId  Missing. Please add all required parameters", "E");
                }
                if (data.FirstName != null && data.FirstName.ToString().Trim() == "")
                {
                    addErrorLog("FirstName  Missing. Please add all required parameters", "E");
                }
                if (data.LastName != null && data.LastName.ToString().Trim() == "")
                {
                    addErrorLog("LastName   Missing. Please add all required parameters", "E");
                }
                if (data.Email != null && data.Email.ToString().Trim() == "")
                {
                    addErrorLog("Email Missing. Please add all required parameters", "E");
                }
                if (data.PhoneNumber != null && data.PhoneNumber.ToString().Trim() == "")
                {
                    addErrorLog("PhoneNumber Missing. Please add all required parameters", "E");
                }
                if (data.IsActive != null && data.IsActive.ToString().Trim() == "")
                {
                    addErrorLog("IsActive Missing. Please add all required parameters", "E");
                }
               
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_userSingleUpdate((data.UserID).ToString(), (data.FirstName).ToString(), (data.LastName).ToString()
                    , (data.Email).ToString(), (data.PhoneNumber).ToString(), (data.IsActive).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/getUserRegistrationDeal_ADO")]
        public List<ReturnData> getUserRegistrationDeal_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
                if (data.UserId != null && data.UserId.ToString().Trim() == "")
                {
                    addErrorLog("UserId Missing. Please add all required parameters", "E");
                }
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_getUserRegistrationDeal((data.UserId).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        [HttpPost]
        [Route("api/{DealReg}/userAddComment_ADO")]
        public List<ReturnData> userAddComment_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null || data.Comments == null || data.DealRegID == null || data.UserId == null)
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }
            
                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_userAddComment((data.UserId).ToString(), (data.Comments).ToString(), (data.DealRegID).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }


        [HttpPost]
        [Route("api/{DealReg}/adminViewUserComments_ADO")]
        public List<ReturnData> adminViewUserComments_ADO([FromBody] dynamic data)
        {
            try
            {
                string lsErrorMsg = "";
                if (data == null  || data.DealRegID == null )
                {
                    addErrorLog("Missing parameters. Please add all required parameters", "E");
                }

                if (ErrorLog.Count > 0)
                {
                    return uiReturnData();
                }
                object loReturn = objDealsADOConnectionDAL.ADO_adminViewUserComments( (data.DealRegID).ToString(), ref lsErrorMsg);
                if (lsErrorMsg != "" && Convert.ToBoolean(loReturn) == false)
                {
                    addErrorLog(lsErrorMsg, "E");
                    return uiReturnData();
                }
                else
                    return uiReturnData(loReturn);
            }
            catch (Exception error)
            {
                addErrorLog(error.Message, "E");
                return uiReturnData();
            }
        }

        public void addErrorLog(string psError, string type = "U", string psErrorDetail = "", string psErrorCode = "")
        {
            ErrorLog.Add(new ErrorLog { Type = type, Error = psError, ErrorCode = psErrorCode, ErrorDetail = psErrorDetail });
        }
        public List<ReturnData> uiReturnData(object lsData = null)
        {
            List<ReturnData> psRet = new List<ReturnData>();
            bool status = ErrorLog.Count > 0 ? false : true;
            psRet.Add(new ReturnData { ErrorLog = ErrorLog, Status = status, Data = lsData });
            return psRet;
        }

    }
}
public class ErrorLog
{
    private string _type;
    private string _Error;
    private string _ErrorDetail;
    private string _ErrorCode;

    public string Type
    {
        get { return _type; }
        set { _type = value; }
    }
    public string ErrorCode
    {
        get { return _ErrorCode; }
        set { _ErrorCode = value; }
    }
    public string ErrorDetail
    {
        get { return _ErrorDetail; }
        set { _ErrorDetail = value; }
    }
    public string Error
    {
        get { return _Error; }
        set { _Error = value; }
    }

}

public class ReturnData
{
    private List<ErrorLog> _ErrorLog = new List<ErrorLog>();
    private bool _Status;
    private object _Data;

    public object Data
    {
        get { return _Data; }
        set { _Data = value; }
    }

    public bool Status
    {
        get { return _Status; }
        set { _Status = value; }
    }

    public List<ErrorLog> ErrorLog
    {
        get { return _ErrorLog; }
        set { _ErrorLog = value; }
    }
}
