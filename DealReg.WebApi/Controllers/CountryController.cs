﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class CountryController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public CountryController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public CountryController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/Countries")]
        //[Authorize(Roles = "Admin")]
        public object Get()
        {
            List<CountryViewModel> CountryViewModel = new List<CountryViewModel>();

            foreach (var item in _unitofwork.CountryRepository.GetAll().ToList())
            {
                CountryViewModel.Add(new CountryViewModel()
                {
                    CountryID = item.CountryID,
                    CountryName = item.CountryName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(CountryViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/CountryByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetCountryByID(int CountryID)
        {

            Country country = _unitofwork.CountryRepository.GetById(CountryID);

            if (country != null)
            {
                CountryViewModel CountryViewModel = new CountryViewModel()
                {
                    CountryID = country.CountryID,
                    CountryName = country.CountryName,
                    CreatedBy = country.CreatedBy,
                    CreatedOn = country.CreatedOn,
                    ModifiedBy = country.ModifiedBy,
                    ModifiedOn = country.ModifiedOn
                };
                return Ok(CountryViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewCountry")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] CountryViewModel CountryViewModel)
        {
            try
            {
                Country country = new Country()
                {
                    CountryName = CountryViewModel.CountryName,
                    CreatedBy = CountryViewModel.CreatedBy,
                    ModifiedBy = CountryViewModel.CreatedBy
                };

                _unitofwork.CountryRepository.Add(country);
                _unitofwork.Save();

                Country existingCountry = _unitofwork.CountryRepository.GetAll().Where(x => x.CountryName == CountryViewModel.CountryName).FirstOrDefault();

                CountryViewModel countryViewModel = new CountryViewModel()
                {
                    CountryID = existingCountry.CountryID,
                    CountryName = existingCountry.CountryName,
                    CreatedBy = existingCountry.CreatedBy,
                    CreatedOn = existingCountry.CreatedOn,
                    ModifiedBy = existingCountry.ModifiedBy,
                    ModifiedOn = existingCountry.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, CountryViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteCountry")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int CountryID)
        {
            try
            {
                if (CountryID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Country recToDelete = new Country() { CountryID = CountryID };

                _unitofwork.CountryRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateCountry")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] CountryViewModel CountryViewModel)
        {
            try
            {
                if (CountryViewModel.CountryName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Country countryUpdate = new Country()
                    {
                        CountryID = CountryViewModel.CountryID,
                        CountryName = CountryViewModel.CountryName,
                        CreatedBy = CountryViewModel.CreatedBy,
                        ModifiedBy = CountryViewModel.ModifiedBy
                    };

                    _unitofwork.CountryRepository.Update(countryUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, countryUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
