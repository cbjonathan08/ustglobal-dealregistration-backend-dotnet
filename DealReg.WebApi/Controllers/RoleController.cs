﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;

namespace DealReg.WebApi.Controllers
{
    public class RoleController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public RoleController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public RoleController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/adminRoles")]
        [Authorize(Roles = "Admin")]
        public object Get()
        {
            //List<Project> projects = db.Projects.Include("ClientLocation").ToList();

            //List<ProjectViewModel> projectsViewModel = new List<ProjectViewModel>();
            //foreach (var project in projects)
            //{
            //    projectsViewModel.Add(new ProjectViewModel() { ProjectID = project.ProjectID, ProjectName = project.ProjectName, TeamSize = project.TeamSize, DateOfStart = project.DateOfStart.ToString("dd/MM/yyyy"), Active = project.Active, ClientLocation = project.ClientLocation, ClientLocationID = project.ClientLocationID, Status = project.Status });
            //}
            //return Ok(projectsViewModel);

            List<RoleViewModel> rolesViewModel = new List<RoleViewModel>();

            foreach (var item in _unitofwork.RoleRepository.GetAll().ToList())
            {
                rolesViewModel.Add(new RoleViewModel()
                {
                    RoleID = item.RoleID,
                    RoleName = item.RoleName,
                    RoleDesc = item.RoleDesc,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(rolesViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/roleByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetProjectByID(int RoleID)
        {
            //Project project = db.Projects.Include("ClientLocation").Where(temp => temp.ProjectID == ProjectID).FirstOrDefault();

            //if (project != null)
            //{
            //    ProjectViewModel projectViewModel = new ProjectViewModel() { ProjectID = project.ProjectID, ProjectName = project.ProjectName, TeamSize = project.TeamSize, DateOfStart = project.DateOfStart.ToString("dd/MM/yyyy"), Active = project.Active, ClientLocation = project.ClientLocation, ClientLocationID = project.ClientLocationID, Status = project.Status };
            //    return Ok(projectViewModel);
            //}
            //else
            //    return new EmptyResult();

            Role role = _unitofwork.RoleRepository.GetById(RoleID);

            if (role != null)
            {
                RoleViewModel projectViewModel = new RoleViewModel() {

                    RoleID = role.RoleID,
                    RoleName = role.RoleName,
                    RoleDesc = role.RoleDesc,
                    CreatedBy = role.CreatedBy,
                    CreatedOn = role.CreatedOn,
                    ModifiedBy = role.ModifiedBy,
                    ModifiedOn = role.ModifiedOn
                };
                return Ok(projectViewModel);
            }
            else
                return null;
        }

        //[HttpPost]
        //[Route("api/projects")]
        //[Authorize]
        //[ValidateAntiForgeryToken]
        //public IActionResult Post([FromBody] Project project)
        //{
        //    project.ClientLocation = null;
        //    db.Projects.Add(project);
        //    db.SaveChanges();

        //    Project existingProject = db.Projects.Include("ClientLocation").Where(temp => temp.ProjectID == project.ProjectID).FirstOrDefault();
        //    ProjectViewModel projectViewModel = new ProjectViewModel() { ProjectID = existingProject.ProjectID, ProjectName = existingProject.ProjectName, TeamSize = existingProject.TeamSize, DateOfStart = existingProject.DateOfStart.ToString("dd/MM/yyyy"), Active = existingProject.Active, ClientLocation = existingProject.ClientLocation, ClientLocationID = existingProject.ClientLocationID, Status = existingProject.Status };

        //    return Ok(projectViewModel);
        //}

        [HttpPost]
        [Route("api/{DealReg}/newRole")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] RoleViewModel roleViewModel)
        {
            try
            {
                Role role = new Role()
                {
                    RoleName = roleViewModel.RoleName,
                    RoleDesc = roleViewModel.RoleDesc,
                    CreatedBy = roleViewModel.CreatedBy,
                    ModifiedBy = roleViewModel.CreatedBy
                };

                _unitofwork.RoleRepository.Add(role);
                _unitofwork.Save();

                Role existingProject = _unitofwork.RoleRepository.GetAll().Where(x => x.RoleName == roleViewModel.RoleName).FirstOrDefault();

                RoleViewModel projectViewModel = new RoleViewModel()
                {
                    RoleID = existingProject.RoleID,
                    RoleName = existingProject.RoleName,
                    RoleDesc = existingProject.RoleDesc,
                    CreatedBy = existingProject.CreatedBy,
                    CreatedOn = existingProject.CreatedOn,
                    ModifiedBy = existingProject.ModifiedBy,
                    ModifiedOn = existingProject.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, projectViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteRole")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int ProjectID)
        {
            //bool result = GlobalConfigFacade.DeleteGlobalCfgItemGroupLocation(igl, User.Identity.Name);

            //if (result)
            //    return Request.CreateResponse(HttpStatusCode.OK);
            //else
            //    return Request.CreateResponse(HttpStatusCode.BadRequest);

            try
            {
                if (ProjectID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                Role userToDelete = new Role() { RoleID = ProjectID };

                _unitofwork.RoleRepository.Delete(userToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);

                //Role role = _unitofwork.RoleRepository.GetById(ProjectID);

                //if (role != null)
                //{
                //    Role userToDelete = new Role() { RoleID = ProjectID };


                //} 
                //else
                //    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateRole")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] RoleViewModel RoleViewModel)
        {
            //bool result = GlobalConfigFacade.UpdateGlobalCfgItemGroupLocation(igls, User.Identity.Name);
            //if (result)
            //    return Request.CreateResponse(HttpStatusCode.OK);
            //else
            //    return Request.CreateResponse(HttpStatusCode.BadRequest);

            try
            {
                if (RoleViewModel.RoleName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    Role roleUpdate = new Role() {
                        RoleID = RoleViewModel.RoleID,
                        RoleName = RoleViewModel.RoleName,
                        RoleDesc = RoleViewModel.RoleDesc,
                        CreatedBy = RoleViewModel.CreatedBy,
                        ModifiedBy = RoleViewModel.ModifiedBy
                    };

                    _unitofwork.RoleRepository.Update(roleUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, roleUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
    }
}
