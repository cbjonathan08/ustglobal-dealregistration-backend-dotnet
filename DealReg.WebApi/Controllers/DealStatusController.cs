﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using DealReg.WebApi.Models;
using System.Security.Claims;
using DealReg.DataModels;
using DealReg.Entities.Entities;


namespace DealReg.WebApi.Controllers
{
    public class DealStatusController : ApiController
    {
        private IUnitOfWork _unitofwork;

        public DealStatusController(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }
        public DealStatusController()
        {
            this._unitofwork = new UnitOfWork();
        }

        [HttpGet]
        [Route("api/{DealReg}/DealStatus")]
        [Authorize(Roles = "Admin")]
        public object Get()
        {
            List<DealStatusViewModel> DealStatusViewModel = new List<DealStatusViewModel>();

            foreach (var item in _unitofwork.DealStatusRepository.GetAll().ToList())
            {
                DealStatusViewModel.Add(new DealStatusViewModel()
                {
                    DealStatusID = item.DealStatusID,
                    DealStatusName = item.DealStatusName,
                    CreatedBy = item.CreatedBy,
                    CreatedOn = item.CreatedOn,
                    ModifiedBy = item.ModifiedBy,
                    ModifiedOn = item.ModifiedOn

                });
            }

            return Ok(DealStatusViewModel);
        }

        [HttpGet]
        [Route("api/{DealReg}/DealStatusByID/{ID}")]
        [Authorize(Roles = "Admin")]
        public object GetCountryByID(int DealStatusID)
        {

            DealStatus dealStatus = _unitofwork.DealStatusRepository.GetById(DealStatusID);

            if (dealStatus != null)
            {
                DealStatusViewModel DealStatusViewModel = new DealStatusViewModel()
                {
                    DealStatusID = dealStatus.DealStatusID,
                    DealStatusName = dealStatus.DealStatusName,
                    CreatedBy = dealStatus.CreatedBy,
                    CreatedOn = dealStatus.CreatedOn,
                    ModifiedBy = dealStatus.ModifiedBy,
                    ModifiedOn = dealStatus.ModifiedOn
                };
                return Ok(DealStatusViewModel);
            }
            else
                return null;
        }

        [HttpPost]
        [Route("api/{DealReg}/NewDealStatus")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage Post([FromBody] DealStatusViewModel DealStatusViewModel)
        {
            try
            {
                DealStatus dealStatus = new DealStatus()
                {
                    DealStatusName = DealStatusViewModel.DealStatusName,
                    CreatedBy = DealStatusViewModel.CreatedBy,
                    ModifiedBy = DealStatusViewModel.CreatedBy
                };

                _unitofwork.DealStatusRepository.Add(dealStatus);
                _unitofwork.Save();

                DealStatus existingDealStatus = _unitofwork.DealStatusRepository.GetAll().Where(x => x.DealStatusName == DealStatusViewModel.DealStatusName).FirstOrDefault();

                DealStatusViewModel dealStatusViewModel = new DealStatusViewModel()
                {
                    DealStatusID = existingDealStatus.DealStatusID,
                    DealStatusName = existingDealStatus.DealStatusName,
                    CreatedBy = existingDealStatus.CreatedBy,
                    CreatedOn = existingDealStatus.CreatedOn,
                    ModifiedBy = existingDealStatus.ModifiedBy,
                    ModifiedOn = existingDealStatus.ModifiedOn
                };

                var message = Request.CreateResponse(HttpStatusCode.Created, DealStatusViewModel);
                return message;
            }
            catch (Exception error)
            {
                var message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
                return message;
            }
        }

        [HttpGet]
        [Route("api/{DealReg}/DeleteDealStatus")]
        [Authorize(Roles = "Admin")]
        public HttpResponseMessage DeletePost(int DealStatusID)
        {
            try
            {
                if (DealStatusID <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Id Not Found");
                }

                DealStatus recToDelete = new DealStatus() { DealStatusID = DealStatusID };

                _unitofwork.DealStatusRepository.Delete(recToDelete);
                _unitofwork.Save();
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, error.Message);
            }
        }

        [Route("api/{DealReg}/UpdateDealStatus")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage PutPatch([FromBody] DealStatusViewModel DealStatusViewModel)
        {
            try
            {
                if (DealStatusViewModel.DealStatusName == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Null");
                }
                else
                {
                    DealStatus dealStatusUpdate = new DealStatus()
                    {
                        DealStatusID = DealStatusViewModel.DealStatusID,
                        DealStatusName = DealStatusViewModel.DealStatusName,
                        CreatedBy = DealStatusViewModel.CreatedBy,
                        ModifiedBy = DealStatusViewModel.ModifiedBy
                    };

                    _unitofwork.DealStatusRepository.Update(dealStatusUpdate);
                    _unitofwork.Save();
                    var message = Request.CreateResponse(HttpStatusCode.Created, dealStatusUpdate);
                    return message;
                }
            }
            catch (Exception error)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }

    }
}
