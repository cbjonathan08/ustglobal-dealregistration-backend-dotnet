﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class TimeLineViewModel
    {
        public int TimeLineID { get; set; }
        public string TimeLineName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}