﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class LoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string token { get; set; }
    }
}