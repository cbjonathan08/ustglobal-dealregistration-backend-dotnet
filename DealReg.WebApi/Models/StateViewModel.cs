﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class StateViewModel
    {
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public string StateName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}