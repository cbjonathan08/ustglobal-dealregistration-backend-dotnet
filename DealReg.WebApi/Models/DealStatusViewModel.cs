﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class DealStatusViewModel
    {
        public int DealStatusID { get; set; }
        public string DealStatusName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}