﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class SlaViewModel
    {
        public int SlaID { get; set; }
        public int RoleID { get; set; }
        public int SLATIME { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}