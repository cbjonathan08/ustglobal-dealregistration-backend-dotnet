﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string memberType { get; set; }
        public string contactNumber { get; set; }
        public string role { get; set; }
        public string token { get; set; }
        public int createdBy { get; set; }
    }
}