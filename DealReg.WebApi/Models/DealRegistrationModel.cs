﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealReg.WebApi.Models
{
    public class DealRegistrationModel
    {
        
        public int DealRegID { get; set; }
        public int UserID { get; set; }
        public int geoID { get; set; }
        public int countryID { get; set; }
        public int stateID { get; set; }
        public int timeLineID { get; set; }
        public DateTime dateOfReg { get; set; }
        public int clientID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string contactPerson { get; set; }
        public string comments  { get; set; }
        public bool IsActive { get; set; }
        public int DealStatusID { get; set; } 
        public int projectID { get; set; }
        public int slaAdmin { get; set; }
        public int slaPartner { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string customerType { get; set; }
        public string StateName { get; set; }
        public string PeerEmails { get; set; }
    }
}