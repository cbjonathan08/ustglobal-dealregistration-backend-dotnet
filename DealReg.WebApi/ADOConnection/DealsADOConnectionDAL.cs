﻿using System;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Web.Configuration;

namespace DealReg.WebApi.ADOConnection
{
    public class DealsADOConnectionDAL
    {
        public static string ContnString;
        public static ProjectRegistry objRegKey;
        public object ADO_GetDealRegInfo( ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_GetDealRegInfo", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_GetDealRegInfoPartner(ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("[usp_GetDealRegInfoPartnerAdmin]", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_GetDealRegInfoWithComments(ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_GetDealRegInfoWithComments", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_UpdateUserComments(string DealId, string DealComment, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_UpdateUserComments", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealId", DealId);
                    ins_cmd.Parameters.AddWithValue("@DealComment", DealComment);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_DealStatusIDCheck(string DealIds,string DealsStatusID,  ref string psErrormsg)
        {
            try
            {
                Initialize();//123
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_DealsApprovalCheck", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealRegID", DealIds);
                    ins_cmd.Parameters.AddWithValue("@DealsStatusID", DealsStatusID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string count = dsResults.Tables[0].Rows[0]["Count"].ToString();
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0 && count != "0")
                    {
                        
                            psErrormsg = "Conatins Deals which are already approved and rejected please select only pending ones";
                            return false;

                        
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_UpdateDealStatusID(string DealIds, string Status, string SlaAdmin, string SlaPartner,string Comments, ref string psErrormsg)
        {
            try
            {
                Initialize();//123
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_UpdateDealStatusID", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealIds", DealIds);
                    ins_cmd.Parameters.AddWithValue("@Status", Status);
                    ins_cmd.Parameters.AddWithValue("@SlaAdmin", SlaAdmin);
                    ins_cmd.Parameters.AddWithValue("@SlaPartner", SlaPartner);
                    ins_cmd.Parameters.AddWithValue("@RoleID", 34);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                        if (dsResults.Tables.Count > 0 && dsResults.Tables[1].Rows.Count > 0)
                        {
                            string errorStr = "";
                            for(int i = 0; i < dsResults.Tables[1].Rows.Count; i++)
                            {
                                string mail_Data = "<p style='font-size:16px;'> Deal details : </p>" 
                                                    +"<p style='font-size:16px;'> Client Name : <b>" + dsResults.Tables[1].Rows[i]["ClientName"].ToString() + "</b></p>"
                                                    + "<p style='font-size:16px;'> Client Address :  <b>" + dsResults.Tables[1].Rows[i]["ClientAddress"].ToString() + "</b></p>"
                                                    + "<p style='font-size:16px;'> Client Contact Person :  <b>" + dsResults.Tables[1].Rows[i]["ClientContactPerson"].ToString() + "</b></p>"
                                                    + "<p style='font-size:16px;'> Deal Comments :  <b>" + dsResults.Tables[1].Rows[i]["UserComments"].ToString() + "</b></p>"
                                                    + "<p style='font-size:16px;'> Deal Satus :  <b>" + dsResults.Tables[1].Rows[i]["DealStatusName"].ToString() + "</b></p>"
                                                    + "<p style='font-size:16px;'> Approval/Rejection Comments:  <b>" + Comments + "</b></p>"
                                                    + "<p style='font-size:16px;'>Thank you</p>";
                                string mail_Body = "<p style='font-size:16px;'>Hi " + dsResults.Tables[1].Rows[i]["FirstName"].ToString() + ",</p>" 
                                    + "<b><p style='font-size:18px'>Admin Changed your deal status</p></b>"+ mail_Data;
                                string mail_Body_Partner = "<p style='font-size:16px;'>Hi  Partner Admin </p>"
                                     + "<b><p style='font-size:18px'>Admin Changed your deal status</p></b>" + mail_Data;

                                bool mailstatus = SendMail(dsResults.Tables[1].Rows[i]["Email"].ToString(), mail_Body, "Admin Changed your deal status", ref errorStr);
                                if(dsResults.Tables[2].Rows.Count > 0 && dsResults.Tables[1].Rows[i]["DealStatusName"].ToString()!="Reject")
                                {
                                    bool mailstatusPartner = SendMail(dsResults.Tables[2].Rows[0]["EMAILS"].ToString(), mail_Body_Partner, "Admin Changed your deal status", ref errorStr);

                                }
                            }
                        }
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_UpdateDealStatusIDPartner(string DealIds, string Status,string Comments, ref string psErrormsg)
        {
            try
            {
                Initialize();//123
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_UpdateDealStatusIDPartner", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealIds", DealIds);
                    ins_cmd.Parameters.AddWithValue("@Status", Status);
                    ins_cmd.Parameters.AddWithValue("@RoleID", 1);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                        if (dsResults.Tables.Count > 0 && dsResults.Tables[1].Rows.Count > 0)
                        {
                            string errorStr = "";
                            for (int i = 0; i < dsResults.Tables[1].Rows.Count; i++)
                            {
                                string mail_Data = "<p style='font-size:16px;'> Deal details : </p>"
                                    + "<p style='font-size:16px;'> Client Name : <b>" + dsResults.Tables[1].Rows[i]["ClientName"].ToString() + "</b></p>"
                                    + "<p style='font-size:16px;'> Client Address :  <b>" + dsResults.Tables[1].Rows[i]["ClientAddress"].ToString() + "</b></p>"
                                    + "<p style='font-size:16px;'> Client Contact Person :  <b>" + dsResults.Tables[1].Rows[i]["ClientContactPerson"].ToString() + "</b></p>"
                                    + "<p style='font-size:16px;'> Deal Comments :  <b>" + dsResults.Tables[1].Rows[i]["UserComments"].ToString() + "</b></p>"
                                    + "<p style='font-size:16px;'> Deal Satus :  <b>" + dsResults.Tables[1].Rows[i]["DealStatusName"].ToString() + "</b></p>"
                                    + "<p style='font-size:16px;'> Approval/Rejection Comments:  <b>" + Comments + "</b></p>"
                                    + "<p style='font-size:16px;'>Thank you</p>";
                                string mail_Body = "<p style='font-size:16px;'>Hi " + dsResults.Tables[1].Rows[i]["FirstName"].ToString() + ",</p>"
                                    + "<b><p style='font-size:18px'>Partner Admin Changed your deal status</p></b>" + mail_Data;
                                string mail_Body_Partner = "<p style='font-size:16px;'>Hi Admin </p>"
                                     + "<b><p style='font-size:18px'>Admin Changed your deal status</p></b>" + mail_Data;
                                bool mailstatus = SendMail(dsResults.Tables[1].Rows[i]["Email"].ToString(), mail_Body, "Partner Admin Changed your deal status", ref errorStr);
                                if (dsResults.Tables[2].Rows.Count > 0)
                                {
                                    bool mailstatusPartner = SendMail(dsResults.Tables[2].Rows[0]["EMAILS"].ToString(), mail_Body_Partner, "Partner Admin Changed your deal status", ref errorStr);

                                }
                            }
                        }
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public void ADO_SendMailToAdmin(Models.DealRegistrationModel DealRegistrationModel, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_getAdminListDetails", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserID", DealRegistrationModel.UserID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        string errorStr = "";
                        string _UserName = "";
                        string adminemails = "";
                        string peerAdminmailMerge = "";
                        for (int i = 0; i < dsResults.Tables[0].Rows.Count; i++)
                        {
                            if (dsResults.Tables[0].Rows[i]["RoleID"].ToString() == "1") {
                                adminemails += dsResults.Tables[0].Rows[i]["Email"].ToString().Trim() + ',';
                            }
                            if (dsResults.Tables[0].Rows[i]["UserID"].ToString() == (DealRegistrationModel.UserID).ToString())
                            {
                                _UserName = dsResults.Tables[0].Rows[i]["FirstName"].ToString().Trim();
                            }

                        }
                        string mail_Body = "<p style='font-size:16px;'>Hi ,</p>"
                                + "<b><p style='font-size:18px'>New Deal waiting for your approval.</p></b>"
                          // + "<p style='font-size:16px;'> Deal Created by : <b>" + _UserName + "</b></p>"
                           + "<p style='font-size:16px;'>Thank you</p>";
                        adminemails = adminemails.Remove(adminemails.Length - 1);
                        if (DealRegistrationModel.PeerEmails.ToString()!="")
                        {
                            peerAdminmailMerge = adminemails + "," + DealRegistrationModel.PeerEmails;

                        }
                        else
                        {
                            peerAdminmailMerge = adminemails;
                        }

                       bool mailstatus = SendMail(peerAdminmailMerge, mail_Body, "New Deal waiting for your approval", ref errorStr);
                    }
                    else
                    {
                        psErrormsg = "No admin emails";
                    }
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
            }
        }
        public object ADO_GetDealMappedSubscribers(string DealRegID, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_GetDealMappedSubscribers", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealRegID", DealRegID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_AddOrRemoveSubscribers(string DealRegID, string UserID, string Status, string CreatedBy, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_AddOrRemoveSubscribers", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealRegID", DealRegID);
                    ins_cmd.Parameters.AddWithValue("@UserID", UserID);
                    ins_cmd.Parameters.AddWithValue("@Status", Status);
                    ins_cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy); 
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                        if (dsResults.Tables.Count > 0 && dsResults.Tables[1].Rows.Count > 0 && dsResults.Tables[2].Rows.Count > 0)
                        {
                            string errorStr = "";
                            if (dsResults.Tables[1].Rows[0]["retype"].ToString() == "sub") {
                                string mail_Body = "<p style='font-size:16px;'>Hi " + dsResults.Tables[1].Rows[0]["FirstName"].ToString() + ",</p>" + "<b><p style='font-size:18px'>You have successfully subscribed a deal.</p></b>"
                                + "<p style='font-size:16px;'> Deal details : </p>"
                               + "<p style='font-size:16px;'> Client Name : <b>" + dsResults.Tables[2].Rows[0]["ClientName"].ToString() + "</b></p>"
                                + "<p style='font-size:16px;'> Client Address :  <b>" + dsResults.Tables[2].Rows[0]["ClientAddress"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'> Client Contact Person :  <b>" + dsResults.Tables[2].Rows[0]["ClientContactPerson"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'> Deal Comments :  <b>" + dsResults.Tables[2].Rows[0]["UserComments"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'>Thank you</p>";

                                
                                bool mailstatus = SendMail(dsResults.Tables[1].Rows[0]["Email"].ToString(), mail_Body, "Admin subscribed a deal for you", ref errorStr);
                            }
                            else if (dsResults.Tables[1].Rows[0]["retype"].ToString() == "unsub") {
                                string mail_Body = "<p style='font-size:16px;'>Hi " + dsResults.Tables[1].Rows[0]["FirstName"].ToString() + ",</p>" + "<b><p style='font-size:18px'>Deal unsubscribed.</p></b>"
                                + "<p style='font-size:16px;'> Deal details : </p>"
                               + "<p style='font-size:16px;'> Client Name : <b>" + dsResults.Tables[2].Rows[0]["ClientName"].ToString() + "</b></p>"
                                + "<p style='font-size:16px;'> Client Address :  <b>" + dsResults.Tables[2].Rows[0]["ClientAddress"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'> Client Contact Person :  <b>" + dsResults.Tables[2].Rows[0]["ClientContactPerson"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'> Deal Comments :  <b>" + dsResults.Tables[2].Rows[0]["UserComments"].ToString() + "</b></p>"
                               + "<p style='font-size:16px;'>Thank you</p>";

                                bool mailstatus = SendMail(dsResults.Tables[1].Rows[0]["Email"].ToString(), mail_Body, "Admin unsubscribed a deal for you", ref errorStr);


                            }
                        }
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_GetDashboardData(string UserID, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("Get_MasterAdminDashboard", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserID", UserID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public bool SendMail(string psTo, string psBody, string psSubject, ref string psErrormsg, string psCC="")
        {
            try
            {
                Initialize();
                if (psTo != "")
                {
                    MailMessage mailMessage = new MailMessage();
                   
                    if (psTo.ToString().Trim() != "")
                    {
                        if (psTo.ToString().Contains(","))
                        {
                            string[] Multi = psTo.ToString().Split(',');
                            foreach(string Multiemailid in Multi)
                            {
                                mailMessage.To.Add(psTo);
                            }
                        }
                        else
                        {
                            mailMessage.To.Add(psTo);
                        }
                        
                    }
                    else
                    {
                        return false;
                    }
                    if ((objRegKey._smtp_cc).ToString().Trim() != "")
                    {
                        mailMessage.CC.Add(objRegKey._smtp_cc);
                    }
                    mailMessage.From = new MailAddress(objRegKey._smtp_email);
                    mailMessage.Subject = psSubject;
                    mailMessage.Body = psBody;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                    smtpClient.Port = 587;
                    smtpClient.Credentials = new System.Net.NetworkCredential(objRegKey._smtp_email, objRegKey._smtp_emailPass);
                    smtpClient.EnableSsl = true;
                    smtpClient.Send(mailMessage);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                System.Diagnostics.Debug.WriteLine(psErrormsg);
                return false;
            }
        }

        public object ADO_GetAdminReportData(string Type, string DealStatusID, string StartDate, string LastDate, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_AdminReport", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@Type", Type);
                    ins_cmd.Parameters.AddWithValue("@DealStatusID", DealStatusID);
                    ins_cmd.Parameters.AddWithValue("@StartDate", StartDate);
                    ins_cmd.Parameters.AddWithValue("@LastDate", LastDate);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_GetUserDashboard(string UserId, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_GetUserDashboard", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_GetMasterTableRecords(string UserId, string Type, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_GetMasterViewTable", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    ins_cmd.Parameters.AddWithValue("@Type", Type);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        
        public object ADO_UserDealFilterByID(string UserId, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("Usp_UserDealFilterByID", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_ChangePassword(string UserId, string OldPassword, string NewPassword, string ModifiedBy, string RoleType, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_UserChangePassword", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    ins_cmd.Parameters.AddWithValue("@OldPassword", OldPassword);
                    ins_cmd.Parameters.AddWithValue("@NewPassword", NewPassword);
                    ins_cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    ins_cmd.Parameters.AddWithValue("@RoleType", RoleType);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_ForgotPassword(string emailID, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("usp_UserForgotPassword", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@emailID", emailID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0 && dsResults.Tables[0].Rows[0]["Code"].ToString() == "10")
                    {
                        string mail_Body = "<p style='font-size:16px;'>Hi,</p>" + "<b><p style='font-size:18px'>Your forgot password request accepted.</p></b>"
                            + "<p style='font-size:16px;'> Your generated OTP is : <b><span style='font-size:18px;color:blue;'>" + dsResults.Tables[0].Rows[0]["OTP"].ToString() + "</span></b> </p>"
                            + "<p style='font-size:16px;'>Thank you</p>";
                        string errorMail = "";
                       bool mailstatus =  SendMail(emailID, mail_Body, "Forgot Password Request", ref errorMail);
                        if (mailstatus)
                        {
                            lsData = JsonConvert.SerializeObject(dsResults);
                        }
                        else
                        {
                            return false;
                        }
                        
                    } else if(dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_AdminGetAllUsers( ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("GetAllUser", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_UpdateUserDetails(string UserId, string FirstName, string LastName, string Email,
            string PhoneNumber, string IsActive, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("UpdateUserDetails", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    ins_cmd.Parameters.AddWithValue("@FirstName", FirstName);
                    ins_cmd.Parameters.AddWithValue("@LastName", LastName);
                    ins_cmd.Parameters.AddWithValue("@Email", Email);
                    ins_cmd.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
                    ins_cmd.Parameters.AddWithValue("@IsActive", IsActive);
                    //ins_cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public object ADO_AdminDeleteUser(string UserId, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("AdminDeleteUser", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);

                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_userGetDetails(string UserId, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("GetSingleUser", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);

                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_userSingleUpdate(string UserId, string FirstName, string LastName, string Email,
            string PhoneNumber, string IsActive, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("UpdateUserDetails_SingleUser", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    ins_cmd.Parameters.AddWithValue("@FirstName", FirstName);
                    ins_cmd.Parameters.AddWithValue("@LastName", LastName);
                    ins_cmd.Parameters.AddWithValue("@Email", Email);
                    ins_cmd.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
                    ins_cmd.Parameters.AddWithValue("@IsActive", IsActive);
                    //ins_cmd.Parameters.AddWithValue("@ModifiedBy", ModifiedBy);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);
                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_getUserRegistrationDeal(string UserId, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("GetUserRegistrationDeal", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);

                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_userAddComment(string UserId, string Comment, string DealRegID, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("UserCreateNewComment", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@UserId", UserId);
                    ins_cmd.Parameters.AddWithValue("@DealRegID", DealRegID);
                    ins_cmd.Parameters.AddWithValue("@Comment", @Comment);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);

                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }

        public object ADO_adminViewUserComments(string DealRegID, ref string psErrormsg)
        {
            try
            {
                Initialize();
                using (SqlConnection sqlConn = new SqlConnection(ContnString))
                {
                    SqlCommand ins_cmd = new SqlCommand("GetUserCommentByDealRegID", sqlConn);
                    ins_cmd.CommandType = CommandType.StoredProcedure;
                    ins_cmd.Parameters.AddWithValue("@DealRegID", DealRegID);
                    sqlConn.Open();
                    DataSet dsResults = new DataSet();
                    var adapter = new SqlDataAdapter(ins_cmd);
                    adapter.Fill(dsResults);
                    sqlConn.Close();
                    string lsData = "";
                    if (dsResults.Tables.Count > 0 && dsResults.Tables[0].Rows.Count > 0)
                    {
                        lsData = JsonConvert.SerializeObject(dsResults);

                    }
                    else
                    {
                        lsData = "[]";
                    }
                    return lsData;
                }
            }
            catch (Exception ex)
            {
                psErrormsg = ex.ToString();
                return false;
            }
        }
        public void Initialize()
        {
            objRegKey = new ProjectRegistry();
            ContnString = objRegKey.ContnString;
        }
    }
    public class ProjectRegistry
    {
        private string _ConnectionString;
        private string _SMTP_EMAILID;
        private string _SMTP_EMAILPASS;
        private string _SMTP_CC;
        public string ContnString
        {
            get { return _ConnectionString = WebConfigurationManager.AppSettings["ConnectionString"]; }
            set { _ConnectionString = WebConfigurationManager.AppSettings["ConnectionString"]; }
        }
        public string _smtp_email
        {
            get { return _SMTP_EMAILID = WebConfigurationManager.AppSettings["smtp_email"]; }
            set { _SMTP_EMAILID = WebConfigurationManager.AppSettings["smtp_email"]; }
        }
        public string _smtp_emailPass
        {
            get { return _SMTP_EMAILPASS = WebConfigurationManager.AppSettings["smtp_emailPass"]; }
            set { _SMTP_EMAILPASS = WebConfigurationManager.AppSettings["smtp_emailPass"]; }
        }
        public string _smtp_cc
        {
            get { return _SMTP_CC = WebConfigurationManager.AppSettings["smtp_cc"]; }
            set { _SMTP_CC = WebConfigurationManager.AppSettings["smtp_cc"]; }
        }

    }
}