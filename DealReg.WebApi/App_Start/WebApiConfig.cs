﻿using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DealReg.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //var corsAttr = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            //config.EnableCors(corsAttr);

            string sourceUrl = System.Configuration.ConfigurationManager.AppSettings["SourceUrl"].ToString();
            var corsAttr = new EnableCorsAttribute(sourceUrl, "*", "*") { SupportsCredentials = true };
            config.EnableCors(corsAttr);

            // Web API routes
            config.MapHttpAttributeRoutes();

            //string sourceUrl = System.Configuration.ConfigurationManager.AppSettings["SourceUrl"].ToString();
            //var cors = new EnableCorsAttribute(sourceUrl, "*", "*") { SupportsCredentials = true };

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //GlobalConfiguration.Configuration.MessageHandlers.Add(new DealRegAPIKeyHandler());
            //config.Filters.Add(new DealRegExceptionAttribute());
        }
    }
}
