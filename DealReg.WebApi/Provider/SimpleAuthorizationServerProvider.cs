﻿using DealReg.Entities.Entities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using DealReg.DataModels;

namespace DealReg.WebApi.Provider
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private IUnitOfWork _unitofwork;
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //   
        }
        public SimpleAuthorizationServerProvider()
        {
            this._unitofwork = new UnitOfWork();
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            //var userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            //var manager = new UserManager<ApplicationUser>(userStore);
            //var user = await manager.FindAsync(context.UserName, context.Password);

            UserRegistration user = _unitofwork.UserRegistrationRepository.GetUserDetails(context.UserName, context.Password);
            if (user != null)
            {
                if (user.RoleID == 1 || (user.Role != null && user.Role.RoleName == "Admin"))
                {
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                    identity.AddClaim(new Claim("ID", user.UserID.ToString()));
                    identity.AddClaim(new Claim("roleid", user.RoleID.ToString()));
                    identity.AddClaim(new Claim("username", user.Email));

                    //return data to client
                    var additionalData = new AuthenticationProperties(new Dictionary<string, string>{
                        {
                            "id",  user.UserID.ToString()
                        },
                        {
                            "roleid", user.RoleID.ToString()
                        },
                        {
                            "username", user.Email
                        },
                        {
                            "Email", user.Email
                        },
                        {
                            "firstName", user.FirstName
                        },
                        {
                            "lastName", user.LastName
                        },
                        {
                            "created_on", user.CreatedOn.ToString()
                        },
                        {
                            "password", ""
                        },
                        {
                            "role", "Admin"
                        }
                    });

                    var token = new AuthenticationTicket(identity, additionalData);
                    context.Validated(token);
                }
                else if (user.RoleID == 34 || (user.Role != null && user.Role.RoleName == "Partner Admin"))
                {
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Partner Admin"));
                    identity.AddClaim(new Claim("ID", user.UserID.ToString()));
                    identity.AddClaim(new Claim("roleid", user.RoleID.ToString()));
                    identity.AddClaim(new Claim("username", user.Email));

                    //return data to client
                    var additionalData = new AuthenticationProperties(new Dictionary<string, string>{
                        {
                            "id",  user.UserID.ToString()
                        },
                        {
                            "roleid", user.RoleID.ToString()
                        },
                        {
                            "username", user.Email
                        },
                        {
                            "Email", user.Email
                        },
                        {
                            "firstName", user.FirstName
                        },
                        {
                            "lastName", user.LastName
                        },
                        {
                            "created_on", user.CreatedOn.ToString()
                        },
                        {
                            "password", ""
                        },
                        {
                            "role", "Partner Admin"
                        }
                    });

                    var token = new AuthenticationTicket(identity, additionalData);
                    context.Validated(token);
                }
                else
                {
                    {
                        var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                        identity.AddClaim(new Claim(ClaimTypes.Role, "Member"));
                        identity.AddClaim(new Claim("ID", user.RoleID.ToString()));
                        identity.AddClaim(new Claim("roleid", user.RoleID.ToString()));
                        identity.AddClaim(new Claim("username", user.Email));

                        //return data to client
                        var additionalData = new AuthenticationProperties(new Dictionary<string, string>{
                             {
                            "id",  user.UserID.ToString()
                            },
                            {
                                "roleid", user.RoleID.ToString()
                            },
                            {
                                "username", user.Email
                            },
                            {
                                "Email", user.Email
                            },
                            {
                                "firstName", user.FirstName
                            },
                            {
                                "lastName", user.LastName
                            },
                            {
                                "created_on", user.CreatedOn.ToString()
                            },
                            {
                                "password", ""
                            },
                            {
                                "role", "Member"
                            }
                        });

                        var token = new AuthenticationTicket(identity, additionalData);
                        context.Validated(token);
                    }
                }
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });
                return;
            }
        }



    }
}