﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_DealStatus", Schema = "ref")]
    public partial class DealStatus
    {
        public DealStatus()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Key]
        public int DealStatusID { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(100)]
        public string DealStatusName { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual IEnumerable<DealRegistration> DealRegistration { get; set; }
        public virtual IEnumerable<DealRegStatusHistory> DealRegStatusHistory { get; set; }

    }
}

