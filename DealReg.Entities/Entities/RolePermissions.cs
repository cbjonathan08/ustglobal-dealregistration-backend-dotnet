﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_RolePermissions", Schema = "gbl")]
    public partial class RolePermissions
    {
        public RolePermissions()
        {
            CreatedOn = DateTime.Now;
        }

        [Key]
        public int RolePermissionID { get; set; }

        [Required]
        public int RoleID { get; set; }

        [Required]
        public bool CanAdd { get; set; }

        [Required]
        public bool CanView { get; set; }

        [Required]
        public bool CanEdit { get; set; }

        [Required]
        public bool CanDelete { get; set; }

        [Required]
        public bool CanNotify { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("RoleID")]
        public virtual Role Role { get; set; }

    }
}

