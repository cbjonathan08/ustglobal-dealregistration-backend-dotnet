﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_State", Schema = "ref")]
    public partial class State
    {
        public State()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Key]
        public int StateID { get; set; }

        [Required]
        public int CountryID { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(100)]
        public string StateName { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

        public virtual IEnumerable<Clients> Clients { get; set; }
        public virtual IEnumerable<DealRegistration> DealRegistration { get; set; }
    }
}

