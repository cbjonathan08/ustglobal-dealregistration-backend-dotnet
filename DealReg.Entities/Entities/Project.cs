﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_Projects", Schema = "ref")]
    public partial class Project
    {
        public Project()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Key]
        public int ProjectID { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string ProjectName { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual IEnumerable<DealRegistration> DealRegistration { get; set; }
    }
}
