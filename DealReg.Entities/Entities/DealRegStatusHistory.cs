﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_DealRegStatusHistory", Schema = "out")]
    public partial class DealRegStatusHistory
    {
        public DealRegStatusHistory()
        {
            CreatedOn = DateTime.Now;
        }

        [Key]
        public int DealRegStatusHistoryID { get; set; }

        [Required]
        public int DealRegID { get; set; }

        [Required]
        public int DealStatusID { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(500)]
        public string Comments { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("DealRegID")]
        public virtual DealRegistration DealRegistration { get; set; }

        [ForeignKey("DealStatusID")]
        public virtual DealStatus DealStatus { get; set; }

    }
}

