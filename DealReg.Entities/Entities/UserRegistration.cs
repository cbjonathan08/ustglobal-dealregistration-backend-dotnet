﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_Users", Schema = "out")]
    public class UserRegistration
    {
        public UserRegistration()
        {
            CreatedOn = DateTime.Now;
        }

        [Key]
        public int UserID { get; set; }

        [Required]
        public int RoleID { get; set; }

        [MaxLength(100)]
        // [StringLength(100)]
        [Column(TypeName ="varchar")]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Required]
        public string LastName { get; set; }

        [MaxLength(100)]
        [Required]
        public string Email { get; set; }

        [MaxLength(100)]
        [Required]
        public string Password { get; set; }

        [NotMapped]
        public string ConfirmPassword { get; set; }

        [MaxLength(30)]
        public string PhoneNumber { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("RoleID")]
        public virtual Role Role { get; set; }

        public virtual IEnumerable<DealRegistration> DealRegistrations { get; set; }

        public virtual IEnumerable<DealRegSubscribers> DealRegSubscribers { get; set; }

    }
}
