﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_DealRegistration", Schema = "out")]
    public partial class DealRegistration
    {
        public DealRegistration()
        {
            CreatedOn = DateTime.Now;
            DateOfReg = DateTime.Now;
        }

        [Key]
        public int DealRegID { get; set; }

        [Required]
        public int UserID { get; set; }

        [Required]
        public int GeoID { get; set; }

        [Required]
        public int CountryID { get; set; }

        [Required]
        public int StateID { get; set; }

        [Required]
        public int TimeLineID { get; set; }

        [Required]
        public DateTime DateOfReg { get; set; }

        public int ClientID { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string ClientName { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(500)]
        public string ClientAddress { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string ClientContactPerson { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(500)]
        public string UserComments { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public int DealStatusID { get; set; }

        [Required]
        public int ProjectID { get; set; }

        public int SlaAdmin { get; set; }

        public int SlaPartner { get; set; }
        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string StateName { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string PeerEmails { get; set; }
        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("UserID")]
        public virtual UserRegistration UserRegistration { get; set; }

        [ForeignKey("GeoID")]
        public virtual Geo Geo { get; set; }

        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

        [ForeignKey("StateID")]
        public virtual State State { get; set; }

        [ForeignKey("TimeLineID")]
        public virtual TimeLine TimeLine { get; set; }

        [ForeignKey("DealStatusID")]
        public virtual DealStatus DealStatus { get; set; }

        [ForeignKey("ProjectID")]
        public virtual Project Project { get; set; }

        //public virtual IEnumerable<DealRegStatusHistory> DealRegStatusHistory { get; set; }
        //public virtual IEnumerable<DealRegComments> DealRegComments { get; set; }
        //public virtual IEnumerable<DealRegSubscribers> DealRegSubscribers { get; set; }



    }
}

