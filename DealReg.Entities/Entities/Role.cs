﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_Roles", Schema ="ref")]
    public partial class Role
    {
        public Role()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Key]
        [Column(Order = 0)]
        public int RoleID { get; set; }

        [Required]
        [Column(Order = 1, TypeName = "varchar")]
        [MaxLength(100)]
        public string RoleName { get; set; }

        [Column(Order =2, TypeName = "varchar")]
        [MaxLength(150)]
        public string RoleDesc { get; set; }

        [Required]
        [Column(Order = 3)]
        public int CreatedBy { get; set; }

        [Required]
        [Column(Order = 4)]
        public DateTime CreatedOn { get; set; }

        [Column(Order = 5)]
        public int? ModifiedBy { get; set; }

        [Column(Order = 6)]
        public DateTime? ModifiedOn { get; set; }

        public virtual IEnumerable<UserRegistration> UserRegistration { get; set; }

        public virtual IEnumerable<RolePermissions> RolePermissions { get; set; }

        public virtual IEnumerable<Sla> Sla { get; set; }
    }
}
