﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace DealReg.Entities.Entities
{
    [Table("tbl_Sla", Schema = "ref")]

    public partial class Sla
    {
        public Sla()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }
        [Key]
        public int SlaID { get; set; }

        [Required]
        public int RoleID { get; set; }

        [Required]
        public int SLATIME { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("RoleID")]
        public virtual Role Role { get; set; }

    }
}
