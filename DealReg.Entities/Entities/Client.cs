﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DealReg.Entities.Entities
{
    [Table("tbl_Clients", Schema = "gbl")]
    public partial class Clients
    {
        public Clients()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
        }

        [Key]
        public int ClientID { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string Name { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [MaxLength(500)]
        public string Address { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(150)]
        public string ContactPerson { get; set; }

        [Required]
        public int GeoID { get; set; }

        [Required]
        public int CountryID { get; set; }

        [Required]
        public int StateID { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [ForeignKey("GeoID")]
        public virtual Geo Geo { get; set; }

        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }

        [ForeignKey("StateID")]
        public virtual State State { get; set; }

    }
}

